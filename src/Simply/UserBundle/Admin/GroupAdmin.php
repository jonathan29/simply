<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 27/11/2015
     * Time: 17:42
     */

    namespace Simply\UserBundle\Admin;

    use Sonata\AdminBundle\Datagrid\ListMapper;
    use Sonata\AdminBundle\Form\FormMapper;
    use Sonata\AdminBundle\Show\ShowMapper;
    use Sonata\UserBundle\Admin\Entity\GroupAdmin as BaseGroupAdmin;


    class GroupAdmin extends BaseGroupAdmin
    {
        protected $translationDomain = 'LocationBundle'; // default is 'messages'

        protected function configureFormFields(FormMapper $formMapper)
        {
//            parent::configureFormFields($formMapper); // TODO: Change the autogenerated stub
            $formMapper
                ->with('Groupes', array('class' => 'col-md-6'))
                ->add('name', NULL, array(
                    'label' => 'Nom',
                ))
                ->add('adresse', NULL, array(
                    'required' => FALSE,
                    'label'    => 'Adresse',
                ))
                ->add('adresseComplement', NULL, array(
                    'label'    => 'Complément d\'adresse',
                    'required' => FALSE,
                ))
                ->add('mail', NULL, array(
                    'required' => FALSE,
                    'label'    => 'Adresse mail',
                ))
                ->add('tel', NULL, array(
                    'required' => FALSE,
                    'label'    => 'Téléphone',
                ))
                ->add('ville', 'sonata_type_model_autocomplete', array(
                        'property' => 'libelle',
                        'required' => FALSE,
                        'attr'     => array(
                            'class' => 'form-control',
                        ),
                    )
                )->remove('roles')
                ->end()
                ->with('Configuration', array('class' => 'col-md-3'))
                ->add('dateDebut', 'sonata_type_date_picker', array(
                    'label' => 'Date de début',
                ))
                ->add('dateFin', 'sonata_type_date_picker', array(
                    'label' => 'Date de fin',
                ))
                ->add('nbCompte')
                ->add(('codeCe'));
//            ->end()
//            ->end();
        }

        protected function configureListFields(ListMapper $listMapper)
        {
            parent::configureListFields($listMapper); // TODO: Change the autogenerated stub
            $listMapper
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show'   => array(),
                            'edit'   => array(),
                            'delete' => array(),
                        ))
                );
        }

        protected function configureShowFields(ShowMapper $showMapper)
        {
            parent::configureShowFields($showMapper); // TODO: Change the autogenerated stub
            $showMapper->add('name')
                ->add('adresse')
                ->add('adresseComplement')
                ->add('mail')
                ->add('tel')
                ->add('roles');
        }


    }