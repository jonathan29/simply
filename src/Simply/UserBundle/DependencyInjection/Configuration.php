<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 30/11/2015
 * Time: 10:31
 */

namespace Simply\UserBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('simply_user');
        return $treeBuilder;
    }


}