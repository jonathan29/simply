<?php
    namespace Simply\UserBundle\Form;

    use Doctrine\Common\Persistence\ObjectManager;
    use Symfony\Component\Form\DataTransformerInterface;
    use Symfony\Component\Form\Exception\TransformationFailedException;

    class EntityToIdTransformer implements DataTransformerInterface
    {
        /**
         * @var ObjectManager
         */
        protected $objectManager;
        /**
         * @var string
         */
        protected $class;

        public function __construct(ObjectManager $objectManager, $class)
        {
            $this->objectManager = $objectManager;
            $this->class = $class;
        }

        public function transform($entity)
        {
            if (NULL === $entity) {
                return;
            }

            return $entity->getId();
        }

        public function reverseTransform($id)
        {
            if (!$id) {
                return NULL;
            }
            $entity = $this->objectManager
                ->getRepository($this->class)
                ->find($id);
            if (NULL === $entity) {
                throw new TransformationFailedException();
            }

            return $entity;
        }
    }