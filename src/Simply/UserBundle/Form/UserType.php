<?php

    namespace Simply\UserBundle\Form;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\Form\FormInterface;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;

    class UserType extends AbstractType
    {
        private $em;

        public function __construct($em)
        {
            $this->em = $em;
        }


        /**
         * @param FormBuilderInterface $builder
         * @param array $options
         */
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
                ->add('firstname', 'text', array(
                    'label' => 'Nom :'
                ))
                ->add('lastname', 'text', array(
                    'label' => 'Prénom :'
                ))
                ->add('dateOfBirth', 'date', array(
                    'label' => 'Date de naissance',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'form-control input-inline datepicker',
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'dd-mm-yyyy'
                    )
                ))
                ->add('email', 'text', array(
                    'label' => 'Adresse Mail'
                ))
                ->add('phone', 'text', array(
                    'label' => 'Téléphone'
                ))
                ->add('adresse', 'text', array(
                    'label' => 'Adresse'
                ))
                ->add('adresseComplement', 'text', array(
                    'label' => 'Complement d\'adresse'
                ))
                ->add('cp', null, array(
                    'mapped' => FALSE,
                    'attr' => array(
                        'class' => 'cp',
                        'maxlength' => 5
                    )
                ));

            $formModifier = function (FormInterface $formInterface, $cp) {
                $villes = $this->em->getRepository('LocationBundle:Ville')->findBy(array('cp' => $cp));

                $formInterface->add('ville', 'entity', array(
                    'class' => 'LocationBundle:Ville',
                    'choices' => $villes
                ));

            };

            $builder->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($formModifier) {
                    $data = $event->getData();
                    $formModifier($event->getForm(), $event->getData());
                }
            );
            $builder->get('cp')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($formModifier) {
                    // It's important here to fetch $event->getForm()->getData(), as
                    // $event->getData() will get you the client data (that is, the ID)
                    $cp = $event->getForm()->getData();

                    // since we've added the listener to the child, we'll have to pass on
                    // the parent to the callback functions!
                    $formModifier($event->getForm()->getParent(), $cp);
                }
            );



        }

        /**                   )
         * @              ));                            param OptionsResolverInterface $resolver
         */
        public
        function setDefaultOptions(OptionsResolverInterface $resolver)
        {
            $resolver->setDefaults(array(
                'data_class' => 'Simply\UserBundle\Entity\User'
            ));
        }

        /**
         * @return string
         */
        public
        function getName()
        {
            return 'simply_userbundle_user';
//            return 'simply_bundles_userbundle_user';
        }
    }
