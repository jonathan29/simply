<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 22/12/2015
     * Time: 09:25
     */

    namespace Simply\UserBundle\Form\Type;

    use FOS\UserBundle\Form\Type\RegistrationFormType as BaseForm;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    
    class RegistrationFormType extends BaseForm
    {
        protected $em;
        protected $request;

        public function __construct($class, $em, Request $request)
        {
            parent::__construct($class);
            $this->em = $em;
            $this->request = $request;
        }

        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            parent::buildForm($builder, $options);
            $builder
            ->add('username',null, array(
                'label'=>'Nom d\'utilisateur',
                    'attr'=>array(
                        'required'=> true,
                        'placeholder'=>'Nom d\'utilisateur'
                        )
                    ))
            ->add('email',null,array(
                'label'=>'Votre adresse email',
                'attr'=> array(
                    'placeholder'=>'adresse email'))
            )
                ->add('firstname', NULL, array(
                    'label' => 'Prénom',
                    'attr'=> array(
                        'placeholder' => 'Votre prénom')
                ))
                ->add('lastname', NULL, array(
                    'label' => 'Nom',
                    'attr'=> array(
                        'placeholder' =>'Votre nom')
                ))
                ->add('codeCe', NULL, array(
                    'mapped'   => FALSE,
                    'required' => TRUE,
                    'attr'=>array(
                        'placeholder'=>'Le code CE de votre entreprise')
                ));
        }

        public function getName()
        {
            return 'simply_user_registration';
        }

    }