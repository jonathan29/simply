<?php

    /*
     * This file is part of the FOSUserBundle package.
     *
     * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
     *
     * For the full copyright and license information, please view the LICENSE
     * file that was distributed with this source code.
     */

    namespace Simply\UserBundle\Form\Handler;

    use Doctrine\ORM\EntityManager;
    use FOS\UserBundle\Form\Handler\RegistrationFormHandler as BaseForm;
    use FOS\UserBundle\Mailer\MailerInterface;
    use FOS\UserBundle\Model\UserInterface;
    use FOS\UserBundle\Model\UserManagerInterface;
    use FOS\UserBundle\Util\TokenGeneratorInterface;
    use Symfony\Component\Security\Core\Exception\AuthenticationException;
    use Symfony\Component\Form\FormInterface;
    use Symfony\Component\HttpFoundation\Request;

    class RegistrationFormHandler extends BaseForm
    {
        protected $em;

        public function __construct(FormInterface $form, Request $request, UserManagerInterface $userManager,
                                    MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator, EntityManager $em)
        {
            parent::__construct($form, $request, $userManager, $mailer, $tokenGenerator);
            $this->em = $em;
        }

        /**
         * @param boolean $confirmation
         */
        public function process($confirmation = FALSE)
        {
            $user = $this->createUser();
            $this->form->setData($user);

            if ('POST' === $this->request->getMethod()) {
                $code = $this->request->request->get('fos_user_registration_form')['codeCe'];
                $group = $this->em->getRepository('SimplyUserBundle:Group')->findGroupByCodeCe($code);

                if(!$group){
                    return false; 
                    die("group");                   
                }

                $nb = $this->em->getRepository('SimplyUserBundle:User')->findNbUserByGroup($group);


                if ($group->getNbCompte() > $nb + 1) {
                    $this->form->bind($this->request);

                    if ($this->form->isValid()) {
                        $this->onSuccess2($user, $confirmation, $group);
                        return TRUE;
                    }
                }
            }
            return FALSE;
        }

        /**
         * @param boolean $confirmation
         */
        protected function onSuccess2(UserInterface $user, $confirmation, $userGroup)
        {
            //On affecte le groupe a l'utilsateur
            $user->setGroup($userGroup);
            if ($confirmation) {
                $user->setEnabled(FALSE);
                if (NULL === $user->getConfirmationToken()) {
                    $user->setConfirmationToken($this->tokenGenerator->generateToken());
                }
                $this->mailer->sendConfirmationEmailMessage($user);
            } else {
                $user->setEnabled(TRUE);
                if (NULL === $user->getConfirmationToken()) {
                    $user->setConfirmationToken($this->tokenGenerator->generateToken());
                }
                $this->mailer->sendConfirmationEmailMessage($user);
            }

            $this->userManager->updateUser($user);
        }

    }
