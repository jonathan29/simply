<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 30/11/2015
     * Time: 10:07
     */

    namespace Simply\UserBundle\Authentication;

    use Doctrine\ORM\EntityManager;
    use Symfony\Component\HttpFoundation\RedirectResponse;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Routing\RouterInterface;
    use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
    use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

    class SuccessHandler implements AuthenticationSuccessHandlerInterface
    {
        protected $router;
        protected $em;

        public function __construct(RouterInterface $router, EntityManager $em)
        {
            $this->router = $router;
            $this->em = $em;
        }

        public function onAuthenticationSuccess(Request $request, TokenInterface $token)
        {
            if ($token->getUser()->isSuperAdmin()) {
                return new RedirectResponse($this->router->generate('sonata_admin_dashboard'));
            } else {
                return new RedirectResponse($this->router->generate('front_homepage'));
            }
        }
    }