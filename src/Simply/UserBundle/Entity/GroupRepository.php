<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 22/12/2015
     * Time: 11:01
     */

    namespace Simply\UserBundle\Entity;

    use Doctrine\ORM\EntityRepository as BaseRepository;

    class GroupRepository extends BaseRepository
    {

        public function findGroupByCodeCe($code)
        {
            $qb = $this->createQueryBuilder('g');

            $qb->where(
                $qb->expr()->eq('g.codeCe', '?1')
            )
                ->setParameter(1, $code);

            return $qb->getQuery()->getSingleResult();
        }

    }