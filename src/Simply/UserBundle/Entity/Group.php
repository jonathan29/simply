<?php

    namespace Simply\UserBundle\Entity;

    use Sonata\UserBundle\Entity\BaseGroup as BaseGroup;

    /**
     * GroupClass
     */
    class Group extends BaseGroup
    {
        /**
         * @var integer
         */
        protected $id;

        /**
         * @var string
         */
        private $adresse;

        /**
         * @var string
         */
        private $adresseComplement;

        /**
         * @var string
         */
        private $tel;

        /**
         * @var string
         */
        private $mail;

        /**
         * @var \Date
         */
        private $dateDebut;

        /**
         * @var \Date
         */
        private $dateFin;

        /**
         * @var \Doctrine\Common\Collections\ArrayCollection
         */
        private $users;

        /**
         * @var integer
         */
        private $nbCompte = '0';


        private $ville;

        /**
         * @var string
         */
        private $password;


//        /**
//         * Constructor
//         */
////        public function __construct()
////        {
////            parent::__construct($this->name, $this->roles = array());
////            $this->users = new \Doctrine\Common\Collections\ArrayCollection();
////        }

        public function __construct()
        {
            parent::__construct($this->name, $this->roles);
            $this->roles = NULL;
        }


        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }


        /**
         * Set adresse
         *
         * @param string $adresse
         *
         * @return GroupClass
         */
        public function setAdresse($adresse)
        {
            $this->adresse = $adresse;

            return $this;
        }

        /**
         * Get adresse
         *
         * @return string
         */
        public function getAdresse()
        {
            return $this->adresse;
        }

        /**
         * Set adresseComplement
         *
         * @param string $adresseComplement
         *
         * @return GroupClass
         */
        public function setAdresseComplement($adresseComplement)
        {
            $this->adresseComplement = $adresseComplement;

            return $this;
        }

        /**
         * Get adresseComplement
         *
         * @return string
         */
        public function getAdresseComplement()
        {
            return $this->adresseComplement;
        }

        /**
         * Set tel
         *
         * @param string $tel
         *
         * @return GroupClass
         */
        public function setTel($tel)
        {
            $this->tel = $tel;

            return $this;
        }

        /**
         * Get tel
         *
         * @return string
         */
        public function getTel()
        {
            return $this->tel;
        }

        /**
         * Set mail
         *
         * @param string $mail
         *
         * @return GroupClass
         */
        public function setMail($mail)
        {
            $this->mail = $mail;

            return $this;
        }

        /**
         * Get mail
         *
         * @return string
         */
        public function getMail()
        {
            return $this->mail;
        }

        /**
         * Set dateDebut
         *
         * @param \DateTime $dateDebut
         *
         * @return GroupClass
         */
        public function setDateDebut($dateDebut)
        {
            $this->dateDebut = $dateDebut;

            return $this;
        }

        /**
         * Get dateDebut
         *
         * @return \DateTime
         */
        public function getDateDebut()
        {
            return $this->dateDebut;
        }

        /**
         * Set dateFin
         *
         * @param \DateTime $dateFin
         *
         * @return GroupClass
         */
        public function setDateFin($dateFin)
        {
            $this->dateFin = $dateFin;

            return $this;
        }

        /**
         * Get dateFin
         *
         * @return \DateTime
         */
        public function getDateFin()
        {
            return $this->dateFin;
        }


        /**
         * Add user
         *
         * @param \Simply\LocationBundle\Entity\UserClass $user
         *
         * @return GroupClass
         */
        public function addUser(\Simply\LocationBundle\Entity\UserClass $user)
        {
            $this->users[] = $user;

            return $this;
        }

        /**
         * Remove user
         *
         * @param \Simply\LocationBundle\Entity\UserClass $user
         */
        public function removeUser(\Simply\LocationBundle\Entity\UserClass $user)
        {
            $this->users->removeElement($user);
        }

        /**
         * Get users
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getUsers()
        {
            return $this->users;
        }


        /**
         * Set ville
         *
         * @param \Simply\LocationBundle\Entity\Ville $ville
         *
         * @return Group
         */
        public function setVille(\Simply\LocationBundle\Entity\Ville $ville = NULL)
        {
            $this->ville = $ville;

            return $this;
        }

        /**
         * Get ville
         *
         * @return \Simply\LocationBundle\Entity\Ville
         */
        public function getVille()
        {
            return $this->ville;
        }

        /**
         * Set nbCompte
         *
         * @param integer $nbCompte
         * @return Group
         */
        public function setNbCompte($nbCompte)
        {
            $this->nbCompte = $nbCompte;

            return $this;
        }

        /**
         * Get nbCompte
         *
         * @return integer
         */
        public function getNbCompte()
        {
            return $this->nbCompte;
        }


        /**
         * Set password
         *
         * @param string $password
         * @return Group
         */
        public function setPassword($password)
        {
            $this->password = $password;

            return $this;
        }

        /**
         * Get password
         *
         * @return string
         */
        public function getPassword()
        {
            return $this->password;
        }

        /**
         * @var string
         */
        private $codeCe;


        /**
         * Set codeCe
         *
         * @param string $codeCe
         * @return Group
         */
        public function setCodeCe($codeCe)
        {
            $this->codeCe = $codeCe;

            return $this;
        }

        /**
         * Get codeCe
         *
         * @return string
         */
        public function getCodeCe()
        {
            return $this->codeCe;
        }

        public function __toString()
        {
            return $this->name != NULL ? $this->name : 'group' ;
        }
    }
