<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 21/12/2015
     * Time: 15:18
     */

    namespace Simply\UserBundle\Entity;


    use Doctrine\ORM\EntityRepository;

    class UserRepository extends EntityRepository
    {
        public function findNbUserByGroup($group)
        {
            $qb = $this->createQueryBuilder('u');

            $qb->select($qb->expr()->count('u'))
                ->leftJoin('u.group', 'g')
                ->where('g = :group')
                ->setParameter('group', $group);

            return $qb->getQuery()->getSingleScalarResult();
        }

    }