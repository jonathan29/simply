<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 25/12/2015
     * Time: 06:35
     */

    namespace Simply\FrontBundle\Validator\Constraint;

    use Symfony\Component\Validator\Constraint;

    class ConstrainstCheckCodeCe extends Constraint
    {
        public $message = "Votr code ce n'est pas valide.";

        public function validatedBy()
        {
            return parent::validatedBy();
        }


    }