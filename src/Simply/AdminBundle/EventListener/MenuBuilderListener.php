<?php
// src/AppBundle/EventListener/MenuBuilderListener.php

namespace Simply\AdminBundle\EventListener;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class MenuBuilderListener extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('location');

//        $menu = $event->getMenu();

        $menu->addChild('Location', array());
        $menu['Location']->addChild('Tours Opérateurs', array(
            'route' => 'admin_location_touroperateur_list'));
        $menu['Location']->addChild('Enseignes', array(
            'route' => 'admin_location_enseigne_list'));
        $menu['Location']->addChild('Hébergements', array(
            'route' => 'admin_location_hebergement_list'));
        $menu['Location']->addChild('Types d\'hébergements', array(
            'route' => 'admin_location_hebergementtype_list'));
        $child->setLabel('Daily and monthly reports');

        return $menu;
    }
}