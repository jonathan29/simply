<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 25/11/2015
 * Time: 11:47
 */

namespace Simply\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class LocationAdmin extends Admin
{
    protected $translationDomain = 'LocationBundle'; // default is 'messages'

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Location')
            ->with('Content', array('class' => 'col-md-9'))
            ->add('hebergement', 'sonata_type_model', array(
                'class' => 'LocationBundle:Hebergement',
                'btn_add' => false
            ))
            ->add('tourOperateur', 'sonata_type_model', array(
                'class' => 'LocationBundle:TourOperateur',
                'btn_add' => false
            ))
            ->add('enseigne', 'sonata_type_model', array(
                'class' => 'LocationBundle:Enseigne',
                'btn_add' => false
            ))
            ->end()
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
//        $datagridMapper->add('tarif');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('enseigne.nom')
            ->add('tourOperateur.nom')

            ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ))
            );
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
//        $showMapper
//            ->add('dateDebut')
//            ->add('dateFin')
//            ->add('tarif');
    }

    public function getExportFields()
    {
        return array('id', 'nom'
        );
    }


    /**
     *
     * @param Location $obj
     * @return type
     *
     * message retourné dans le breadcumb
     */
    public function toString($obj)
    {
        return $obj instanceof TourOperateur ? $obj->getName() : 'Enseigne';
    }
}