<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 25/11/2015
 * Time: 11:33
 */

namespace Simply\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class EnseigneImageAdmin extends Admin
{
    protected $translationDomain = 'LocationBundle'; // default is 'messages'

    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->hasParentFieldDescription()) { // this Admin is embedded
            // $getter will be something like 'getlogoImage'
            $getter = 'get' . $this->getParentFieldDescription()->getFieldName();

            // get hold of the parent object
            $parent = $this->getParentFieldDescription()->getAdmin()->getSubject();
            if ($parent) {
                $image = $parent->$getter();
            } else {
                $image = null;
            }
        } else {
            $image = $this->getSubject();
        }
        $fileFieldOptions = array('required' => false);

        if ($image && ($webPath = $image->getWebPath())) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath() . '/' . $webPath;
            $fileFieldOptions['help'] = '<img src="' . $fullPath . '" class="admin-preview"/>';
        }
        $formMapper->add('file', 'file', $fileFieldOptions, array(
            'required' => FALSE,
        ));
    }


    /**
     *
     * @param Location $obj
     * @return type
     *
     * message retourné dans le breadcumb
     */
    public function toString($obj)
    {
        return $obj instanceof TourOperateur ? $obj->getName() : 'Image de enseigne';
    }
}