<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 25/11/2015
     * Time: 11:47
     */

    namespace Simply\AdminBundle\Admin;

    use Sonata\AdminBundle\Admin\Admin;
    use Sonata\AdminBundle\Admin\AdminInterface;
    use Sonata\AdminBundle\Datagrid\DatagridMapper;
    use Sonata\AdminBundle\Datagrid\ListMapper;
    use Sonata\AdminBundle\Form\FormMapper;
    use Sonata\AdminBundle\Show\ShowMapper;

    class ReservationAdmin extends Admin
    {
        protected $translationDomain = 'LocationBundle'; // default is 'messages'

        protected function configureFormFields(FormMapper $formMapper)
        {
            $formMapper
                ->with('Réservation', array('class' => 'col-md-9'))
                ->add('etat', 'entity', array(
                    'class' => 'LocationBundle:Etat'
                ))
                ->end();

        }

        protected function configureDatagridFilters(DatagridMapper $datagridMapper)
        {
            $datagridMapper->add('etat')
                ->add('user');
        }

        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->addIdentifier('id')
                ->add('date', null, array(
                    'label' => 'Date de réservation'
                ))
//                ->add('etat')
                ->add('etat', null, array('editable' => true))

                ->add('offreLocation.dateDebut', null, array(
                    'label' => 'Début de la réservation'
                ))
                ->add('offreLocation.location.enseigne', null, array(
                    'label' => 'Enseigne'
                ))
                ->add('offreLocation.location.hebergement', null, array(
                    'label' => 'Hébergement   '
                ))
                ->add('user', null, array(
                    'label' => 'Utilisateur'
                ))
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show' => array(),
                            'edit' => array()
                        ))
                );
        }

        protected function configureShowFields(ShowMapper $showMapper)
        {
            // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
            $showMapper
                ->with('Reservation', array('class' => 'col - md - 9'))
                ->add('date', null, array(
                    'label' => 'Date de la réservation'
                ))
                ->add('etat', null, array(
                    'label' => 'Etat de la réservation'
                ))
                ->end()->with('Utilisateur', array('class' => 'col - md - 9'))
                ->add('user', null, array(
                    'label' => 'Utilisateur',
                ))->end()
                ->with('Offre de Location', array('class' => 'col - md - 9'))->add('offreLocation . dateDebut')->add
                ('offreLocation . dateFin')->add('offreLocation . tarif')->end()
                ->with('Enseigne', array('class' => 'col - md - 9'))
                ->add('offreLocation . location . enseigne', null, array(
                    'label' => 'Enseigne'
                ))->end();

        }

        public function getExportFields()
        {
            return array('id', 'etat', 'date', 'offreLocation . dateDebut', 'offreLocation . location . enseigne',
            );
        }


        /**
         *
         * @param Location $obj
         * @return type
         *
         * message retourné dans le breadcumb
         */
        public function toString($obj)
        {
            return $obj instanceof TourOperateur ? $obj->getName() : 'Réservation';
        }
    }