<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 25/11/2015
 * Time: 11:47
 */

namespace Simply\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OffreLocationAdmin extends Admin
{
    protected $translationDomain = 'LocationBundle'; // default is 'messages'

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Enseigne')
            ->with('Content', array('class' => 'col-md-9'))
            ->add('tarif')
            ->add('enabled', 'checkbox', array(
                'required' => FALSE,
                'label' => 'Etat'))
            ->add('dateDebut', 'sonata_type_date_picker', array(
                'label' => 'Date de début de la réservation',
                'format' => 'dd/MM/yyyy'))
            ->add('dateFin', 'sonata_type_date_picker', array(
                'label' => 'Date de fin',
                'format' => 'dd/MM/yyyy'))
            ->add('modeReservation', 'sonata_type_model', array(
                'class' => 'LocationBundle:ReservationMode',
                'btn_add' => FALSE
            ))
            ->end()
            ->with('Infos', array('class' => 'col-md-3'))
            ->add('location', 'sonata_type_admin', array(
                'btn_add' => FALSE
            ))
            ->end()
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('tarif', 'doctrine_orm_number', array(
                'field_type' => 'sonata_type_filter_number'),
                null,
                array()
            )
            ->add('location.enseigne.nom', null, array(
                'label' => 'Enseigne'
            ))
            ->add('dateDebut', 'doctrine_orm_date_range', array(
                'field_type' => 'sonata_type_date_range_picker',
                'input_type' => 'date'), null, array())
            ->add('dateFin', 'doctrine_orm_date_range', array
            ('field_type' => 'sonata_type_date_range_picker',),
                null,
                array()
            );
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('location.enseigne.nom', null, array(
                'label' => 'Enseigne'
            ))
            ->add('location.tourOperateur.nom', null, array(
                'label' => 'Tour opérateur'
            ))
            ->add('location.hebergement.libelle', null, array(
                'label' => 'Hébergement'
            ))
            ->add('dateDebut', 'date', array(
                'format' => 'd-m-Y',
                'label' => 'Debut'
            ))
            ->add('dateFin', 'date', array(
                'format' => 'd-m-Y',
                'label' => 'Fin'
            ))
            ->add('tarif')
            ->add('enabled', null, array(
                'editable' => TRUE,
                'label' => 'etat'
            ))
            ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ))
            );
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper
            ->add('dateDebut', 'date', array(
                'label' => 'Début de la location',
                'format' => 'd-m-y'
            ))
            ->add('dateFin', 'date', array(
                'label' => 'Fin de la location',
                'format' => 'd-m-y'))
            ->add('tarif');
    }


    /**
     *
     * @param Location $obj
     * @return type
     *
     * message retourné dans le breadcumb
     */
    public function toString($obj)
    {
        return $obj instanceof TourOperateur ? $obj->getName() : 'Enseigne';
    }
}