<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 25/11/2015
     * Time: 11:47
     */

    namespace Simply\AdminBundle\Admin;

    use Sonata\AdminBundle\Admin\Admin;
    use Sonata\AdminBundle\Datagrid\DatagridMapper;
    use Sonata\AdminBundle\Datagrid\ListMapper;
    use Sonata\AdminBundle\Form\FormMapper;
    use Sonata\AdminBundle\Show\ShowMapper;

    class HebergementAdmin extends Admin
    {
        protected $translationDomain = 'LocationBundle'; // default is 'messages'

        protected function configureFormFields(FormMapper $formMapper)
        {
            $formMapper
                ->tab('Hebergement')
                ->with('Content', array('class' => 'col-md-9'))
                ->add('libelle')
                ->add('description', null, array(
                    'required' => FALSE,
                ))
                ->add('surface', null, array(
                    'required' => FALSE
                ))
                ->add('nbChambre', null, array(
                    'required' => FALSE,
                    'label' => 'Nombre de chambres'
                ))
                ->add('nbPiece', null, array(
                    'required' => FALSE,
                    'label' => 'Nombre de pièces'
                ))
                ->add('nbMaxPlace', null, array(
                    'required' => FALSE,
                    'label' => 'Nombre de places maximum'
                ))
                ->add('nbPlace', null, array(
                    'required' => FALSE,
                    'label' => 'Nombre de places'
                ))
                ->end()
                ->end()
                ->tab('Type d\'hébergement')
                ->with('Content', array('class' => 'col-md-9'))
                ->add('hebergementType', 'sonata_type_model', array(
                    'class' => 'LocationBundle:HebergementType',
                    'btn_add' => TRUE
                ))
//            ->add('hebergementType', 'entity', array(
//                'class' => 'Simply\LocationBundle\Entity\Hebergement',
//                'property' => 'libelle',
//                'btn_add' => true))
//            ->add('hebergementType', 'sonata_type_admin', array(
//                'btn_add' => false
//            ))
                ->end()
                ->end();
        }

        protected function configureDatagridFilters(DatagridMapper $datagridMapper)
        {
            $datagridMapper->add('libelle')
                ->add('surface')
                ->add('nbChambre')
                ->add('nbPiece')
                ->add('nbMaxPlace')
                ->add('nbPlace');
        }

        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->addIdentifier('libelle')
                ->add('surface')
                ->add('nbChambre')
                ->add('nbPiece')
                ->add('nbMaxPlace')
                ->add('nbPlace')
                ->add('hebergementType.libelle', null, array(
                    'label' => 'Type d\'hébergement'
                ))
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show' => array(),
                            'edit' => array(),
                            'delete' => array(),
                        ))
                );
        }

        protected function configureShowFields(ShowMapper $showMapper)
        {
            // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
            $showMapper
                ->add('libelle')
                ->add('description')
                ->add('surface')
                ->add('nbChambre')
                ->add('nbPiece')
                ->add('nbMaxPlace')
                ->add('nbPlace')
                ->add('hebergementType.libelle', null, array(
                    'label' => 'Type d\'hébergement'
                ));

        }

        /**
         *
         * @param Location $obj
         * @return type
         *
         * message retourné dans le breadcumb
         */
        public function toString($obj)
        {
            return $obj instanceof TourOperateur ? $obj->getName() : 'Hébergement';
        }
    }