<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 25/11/2015
     * Time: 11:47
     */

    namespace Simply\AdminBundle\Admin;

    use Sonata\AdminBundle\Admin\Admin;
    use Sonata\AdminBundle\Admin\AdminInterface;
    use Sonata\AdminBundle\Datagrid\DatagridMapper;
    use Sonata\AdminBundle\Datagrid\ListMapper;
    use Sonata\AdminBundle\Form\FormMapper;
    use Sonata\AdminBundle\Show\ShowMapper;

    class EnseigneAdmin extends Admin
    {
        protected $translationDomain = 'LocationBundle'; // default is 'messages'

        protected function configureFormFields(FormMapper $formMapper)
        {
            $formMapper
                ->tab('Enseigne')
                ->with('Content', array('class' => 'col-md-9'))
                ->add('nom', NULL, array(
                    'required' => FALSE,
                    'label'    => 'Nom de l\'enseigne',
                ))
                ->add('classement', NULL, array(
                    'required' => FALSE,
                ))
                ->add('etat', NULL, array(
                    'label'    => 'Etat',
                    'required' => FALSE,
                ))
                ->add('description')
                ->add('mail', NULL, array(
                    'label'    => 'Adresse mail',
                    'required' => FALSE,
                ))
                ->add('tel', NULL, array(
                    'label'    => 'Téléphone',
                    'required' => FALSE,
                ))
                ->add('debutSaison', 'sonata_type_date_picker')
                ->add('finSaison', 'sonata_type_date_picker')
                ->add('latitude')
                ->add('longitude')
                ->add('adresse', NULL, array(
                    'required' => FALSE,
                    'label'    => 'Adresse',
                ))
                ->add('adresseComplement', NULL, array(
                    'required' => FALSE,
                    'label'    => 'Compément d\'adresse',
                ))
                ->add('ville', 'sonata_type_model_autocomplete', array(
                    'property' => 'libelle',
                ))
                ->end()
                ->end()
                ->tab('Images')
                ->with('images')
                ->add('image', 'sonata_type_admin', array(
                    'btn_add'    => FALSE,
                    'btn_delete' => TRUE,
                ))
                ->end()
                ->end()
                ->tab('Theme')
                ->add('theme', 'sonata_type_model', array(
                    'class'    => 'LocationBundle:EnseigneTheme',
                    'btn_add'  => TRUE,
                    'required' => FALSE,
                ))
                ->end()
                ->end()
                ->tab('Options')
                ->add('options', NULL, array(
                    'multiple' => 'true',
                ))
                ->end();

        }

        protected function configureDatagridFilters(DatagridMapper $datagridMapper)
        {
            $datagridMapper
                ->add('nom')
                /*->add('classement', 'doctrine_orm_number', array(
                    'field_type' => 'sonata_type_filter_number'), NULL, array()
                )
                ->add('debutSaison', 'doctrine_orm_date_range', array(
                    'field_type' => 'sonata_type_date_range_picker',), NULL, array())
                ->add('finSaison', 'doctrine_orm_date_range', array
                ('field_type' => 'sonata_type_date_range_picker',), NULL, array())*/;
        }

        protected function configureListFields(ListMapper $listMapper)
        {

            $listMapper
                ->addIdentifier('id')
                ->add('nom')
                ->add('classement', NULL, array(
                    'label' => 'Etoiles',
                ))
                ->add('image', NULL, array(
                    'template' => 'AdminBundle:HebergementType:image-list.html.twig',
                ))
                ->add('etat', NULL, array(
                    'editable' => TRUE,
                ))
                ->add('debutSaison', NULL, array(
                    'label'   => 'Début de la saison',
                    'pattern' => 'dd-mm-yyyy',
                ))
                ->add('finSaison', 'date', array(
                    'pattern' => 'dd-mm-yyyy',
                    'label'   => 'Fin de la saison',
                ))
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show'   => array(),
                            'edit'   => array(),
                            'delete' => array(),
                        ))
                );
        }

        protected function configureShowFields(ShowMapper $showMapper)
        {
            // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
            $showMapper
                ->add('nom')
                ->add('classement', NULL, array(
                    'label' => 'Etoiles',
                ))
                ->add('description')
                ->add('mail', NULL, array(
                    'label' => 'Adresse mail',
                ))
                ->add('tel', NULL, array(
                    'label' => 'Numéro de téléphone',
                ))
                ->add('debutSaison', NULL, array(
                    'label' => 'Jour d\'ouverture',
                ))
                ->add('finSaison', NULL, array(
                    'label' => 'Jour de fermeture',
                ))
                ->add('latitude')
                ->add('longitude')
                ->add('image', NULL, array(
                    'template' => 'AdminBundle:HebergementType:image-show.html.twig',
                ));;
        }

        public function getExportFields()
        {
            return array('id', 'nom',
            );
        }


        public function preUpdate($objectif)
        {
            // i get the _delete variable of my sonata_type_admin Image widget
            $params = $this->getRequest()->request->get($this->getUniqid());
            if (isset ($params['image']) && !empty ($params['image'])) {
                $image = $params['image'];

                // if the checkbox is checked i set NULL to my objectif image
                if (isset ($image['_delete']) && !empty ($image['_delete'])) {
                    $objectif->setImage(NULL);
                } else {
                    //$objectif->setImage($objectif->getImage());
                    $objectif->addImage($objectif->getImage());

                }
            }
            $this->manageEmbeddedImageAdmins($objectif);
        }

        private function manageEmbeddedImageAdmins($page)
        {
            // Cycle through each field
            foreach ($this->getFormFieldDescriptions() as $fieldName => $fieldDescription) {

                // detect embedded Admins that manage Images
                if ($fieldDescription->getType() === 'sonata_type_admin' &&
                    ($associationMapping = $fieldDescription->getAssociationMapping()) &&
                    $associationMapping['targetEntity'] === 'Simply\LocationBundle\Entity\EnseigneOptionImage'
                ) {
                    $getter = 'get' . $fieldName;
                    $setter = 'set' . $fieldName;

                    /** @var Image $image */
                    $image = $page->$getter();
                    if ($image) {
                        if ($image->getFile()) {
                            // update the Image to trigger file management
//                            $image->refreshUpdated();
                        } elseif (!$image->getFile() && !$image->getUrl()) {
                            // prevent Sf/Sonata trying to create and persist an empty Image
                            $page->$setter(NULL);
                        }
                    }
                }
            }
        }


        /**
         *
         * @param Location $obj
         * @return type
         *
         * message retourné dans le breadcumb
         */
        public function toString($obj)
        {
            return $obj instanceof TourOperateur ? $obj->getName() : 'Enseigne';
        }
    }