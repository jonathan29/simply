<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 25/11/2015
     * Time: 11:47
     */

    namespace Simply\AdminBundle\Admin;

    use Sonata\AdminBundle\Admin\Admin;
    use Sonata\AdminBundle\Admin\AdminInterface;
    use Sonata\AdminBundle\Datagrid\DatagridMapper;
    use Sonata\AdminBundle\Datagrid\ListMapper;
    use Sonata\AdminBundle\Form\FormMapper;
    use Sonata\AdminBundle\Show\ShowMapper;

    class EnseigneThemeAdmin extends Admin
    {
        protected $translationDomain = 'LocationBundle'; // default is 'messages'

        protected function configureFormFields(FormMapper $formMapper)
        {
            $formMapper
                ->tab('Theme')
                ->with('Content', array('class' => 'col-md-9'))
                ->add('libelle', NULL, array(
                    'required' => FALSE,
                ))
                ->add('etat', 'choice', array(
                    'choices' => array(
                        TRUE  => 'Active',
                        FALSE => 'Inactif',
                    ),
                ))
                ->add('image', 'sonata_type_admin', array(
                    'btn_delete' => TRUE,
                    'btn_add'    => FALSE,
                ))
                ->end()
                ->end();
        }

        protected function configureDatagridFilters(DatagridMapper $datagridMapper)
        {
            $datagridMapper->add('libelle')
                ->add('etat');
        }

        protected function configureListFields(ListMapper $listMapper)
        {
            $listMapper
                ->addIdentifier('id')
                ->add('libelle')
                ->add('etat', NULL, array('editable' => TRUE))
                ->add('image', NULL, array(
                    'template' => 'AdminBundle:HebergementType:image-list.html.twig',
                ))
                ->add('_action', 'actions', array(
                        'actions' => array(
                            'show'   => array(),
                            'edit'   => array(),
                            'delete' => array(),
                        ))
                );
        }

        protected function configureShowFields(ShowMapper $showMapper)
        {
            // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
            $showMapper
                ->add('id')
                ->add('libelle')
                ->add('etat')
                ->add('image', NULL, array(
                    'template' => 'AdminBundle:HebergementType:image-show.html.twig',
                ));;
        }

        public function getExportFields()
        {
            return array(
                'id',
                'libelle',
                'etat',
            );
        }

        public function preUpdate($objectif)
        {
            // i get the _delete variable of my sonata_type_admin Image widget
            $params = $this->getRequest()->request->get($this->getUniqid());
            if (isset ($params['image']) && !empty ($params['image'])) {
                $image = $params['image'];

                // if the checkbox is checked i set NULL to my objectif image
                if (isset ($image['_delete']) && !empty ($image['_delete'])) {
                    $objectif->setImage(NULL);
                } else {
                    $objectif->setImage($objectif->getImage());
                }
            }
            $this->manageEmbeddedImageAdmins($objectif);
        }

        private function manageEmbeddedImageAdmins($page)
        {
            // Cycle through each field
            foreach ($this->getFormFieldDescriptions() as $fieldName => $fieldDescription) {

                // detect embedded Admins that manage Images
                if ($fieldDescription->getType() === 'sonata_type_admin' &&
                    ($associationMapping = $fieldDescription->getAssociationMapping()) &&
                    $associationMapping['targetEntity'] === 'Simply\LocationBundle\Entity\EnseigneOptionImage'
                ) {
                    $getter = 'get' . $fieldName;
                    $setter = 'set' . $fieldName;

                    /** @var Image $image */
                    $image = $page->$getter();
                    if ($image) {
                        if ($image->getFile()) {
                            // update the Image to trigger file management
//                            $image->refreshUpdated();
                        } elseif (!$image->getFile() && !$image->getUrl()) {
                            // prevent Sf/Sonata trying to create and persist an empty Image
                            $page->$setter(NULL);
                        }
                    }
                }
            }
        }

        /**
         *
         * @param Location $obj
         * @return type
         *
         * message retourné dans le breadcumb
         */
        public function toString($obj)
        {
            return $obj instanceof TourOperateur ? $obj->getName() : 'Enseigne';
        }
    }