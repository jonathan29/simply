<?php
// src/AppBundle/Admin/CategoryAdmin.php
namespace Simply\AdminBundle\Admin;

use Knp\Menu\ItemInterface;
use Simply\LocationBundle\Entity\TourOperateur;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TourOperateurAdmin extends Admin
{
    protected $translationDomain = 'LocationBundle'; // default is 'messages'

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Location')
            ->with('Content', array('class' => 'col-md-9'))
            ->add('nom', 'text')
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('nom');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nom')
            ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ))
            );
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper
            ->add('nom');
    }

    public function getExportFields()
    {
        return array('nom');
    }

    /**
     *
     * @param Location $obj
     * @return type
     *
     * message retourné dans le breadcumb
     */
    public function toString($obj)
    {
        return $obj instanceof TourOperateur ? $obj->getName() : 'Tour Opérateur';
    }
}