<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 14/12/15
 * Time: 20:43
 */

namespace Simply\AdminBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use  Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
class VilleAdmin extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('libelle')->add('cp')->add('latitude')->add('longitude');

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('libelle')
            ->add('cp');
    }

    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->addIdentifier('id')
            ->add('libelle')
            ->add('cp');
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper
            ->add('libelle')
            ->add('cp')
            ->add('latitude')
            ->add('longitude')
            ->add('image', null, array(
                'template' => 'AdminBundle:HebergementType:image-show.html.twig'
            ));;
    }

//    public function getExportFields()
//    {
//        return array('id', 'nom'
//        );
//    }


    /**
     *
     * @param Location $obj
     * @return type
     *
     * message retourné dans le breadcumb
     */
    public function toString($obj)
    {
//        return $obj instanceof Ville ? $obj->geLibelle() : 'Ville';
        return $obj->getLibelle();
    }

}