<?php

    namespace Simply\LocationBundle;

    use Simply\LocationBundle\DependencyInjection\LocationExtension;
    use Symfony\Component\HttpKernel\Bundle\Bundle;

    class LocationBundle extends Bundle
    {
        public function getContainerExtension()
        {
            return new LocationExtension();
        }
    }