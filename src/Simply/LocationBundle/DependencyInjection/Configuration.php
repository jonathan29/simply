<?php
    namespace Simply\LocationBundle\DependencyInjection;

    use Symfony\Component\Config\Definition\Builder\TreeBuilder;
    use Symfony\Component\Config\Definition\ConfigurationInterface;

    /**
     * This is the class that validates and merges configuration from your app/config files
     *
     * To learn more see {@link
     * http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
     */
    class Configuration implements ConfigurationInterface
    {


        public function getConfigTreeBuilder()
        {
            $treeBuilder = new TreeBuilder();
            $rootNode = $treeBuilder->root('LocationBundle');

            $rootNode
                ->children()
                // Driver used by the resource bundle
                ->scalarNode('driver')->defaultValue('doctrine/orm')->end()
//            ->scalarNode('driver')->isRequired()->cannotBeEmpty()->end()
                // Object manager used by the resource bundle, if not specified "default" will used
                ->scalarNode('object_manager')->defaultValue('default')->end()
                // Validation groups used by the form component
                ->arrayNode('validation_groups')
                ->addDefaultsIfNotSet()
                ->children()
                ->arrayNode('touroperateur')
                ->prototype('scalar')->end()
                ->defaultValue(array('location'))
                ->end()
                ->end()
                ->end()
                // Configure the template namespace used by each resource
                ->arrayNode('templates')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('HebergementType')->defaultValue('LocationBundle:Backend/HebergementType')->end()
                ->scalarNode('my_other_entity')->defaultValue('MyOtherCoreBundle:Entity')->end()
                ->end()
                ->end()
                // The resources
                ->arrayNode('classes')
                ->addDefaultsIfNotSet()
                ->children()
                ->arrayNode('UserClass')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('model')->defaultValue('Simply\LocationBundle\Entity\UserClass')->end()
                ->scalarNode('controller')->defaultValue('Sylius\Bundle\ResourceBundle\Controller\ResourceController')->end()
                ->scalarNode('repository')->/*defaultValue('Simply\LocationBundle\Entity\UserClassRepository')->*/
                end()
                ->scalarNode('form')->end()
                ->end()
                ->end()
                ->arrayNode('HebergementTypeImage')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('model')->defaultValue('Simply\LocationBundle\Entity\HebergementTypeImage')->end()
                ->scalarNode('controller')->defaultValue('Sylius\Bundle\ResourceBundle\Controller\ResourceController')->end()
                ->scalarNode('repository')->end()
                ->scalarNode('form')->defaultValue('Simply\LocationBundle\Form\Type\HebergementTypeImageType')->end()
                ->end()
                ->end()
                ->arrayNode('HebergementType')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('model')->defaultValue('Simply\LocationBundle\Entity\HebergementType')->end()
                ->scalarNode('controller')->defaultValue('Sylius\Bundle\ResourceBundle\Controller\ResourceController')->end()
                ->scalarNode('repository')->end()
                ->scalarNode('form')->defaultValue('Simply\LocationBundle\Form\Type\HebergementTypeFormType')->end()
                ->end()
                ->end()
                ->arrayNode('Hebergement')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('model')->defaultValue('Simply\LocationBundle\Entity\Hebergement')->end()
                ->scalarNode('controller')->defaultValue('Sylius\Bundle\ResourceBundle\Controller\ResourceController')->end()
                ->scalarNode('form')->defaultValue('Simply\LocationBundle\Form\Type\HebergementFormType')->end()
                // you can use an array, useful when you want to register the choice form type.
                /*     ->arrayNode('form')
                         ->addDefaultsIfNotSet()
                         ->children()
                             ->scalarNode('default')->defaultValue('MyApp\MyCustomBundle\Form\Type\MyformType')->end()
                             ->scalarNode('choice')->defaultValue('MyApp\MyCustomBundle\Form\Type\MyChoiceformType')->end()
                         ->end()
                     ->end()*/
                ->end()
                ->end()
                ->arrayNode('TourOperateur')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('model')->defaultValue('Simply\LocationBundle\Entity\TourOperateur')->end()
                ->scalarNode('controller')->defaultValue('Sylius\Bundle\ResourceBundle\Controller\ResourceController')->end()
                ->scalarNode('repository')->end()
                ->scalarNode('form')->defaultValue('Simply\LocationBundle\Form\Type\TourOperateurType')->end()
                ->end()
                ->end()
                ->arrayNode('Enseigne')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('model')->defaultValue('Simply\LocationBundle\Entity\Enseigne')->end()
                ->scalarNode('controller')->defaultValue('Sylius\Bundle\ResourceBundle\Controller\ResourceController')->end()
                ->scalarNode('repository')->end()
                ->scalarNode('form')->defaultValue('Simply\LocationBundle\Form\Type\EnseigneType')->end()
                ->end()
                ->end()
                ->arrayNode('OffreLocation')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('model')->defaultValue('Simply\LocationBundle\Entity\OffreLocation')->end()
                ->scalarNode('controller')->defaultValue('Sylius\Bundle\ResourceBundle\Controller\ResourceController')->end()
                ->scalarNode('repository')->end()
                ->scalarNode('form')->defaultValue('Simply\LocationBundle\Form\Type\OffreLocationType')->end()
                ->end()
                ->end()
                ->arrayNode('ModeReservation')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('model')->defaultValue('Simply\LocationBundle\Entity\ReservationMode')->end()
                ->scalarNode('controller')->defaultValue('Sylius\Bundle\ResourceBundle\Controller\ResourceController')->end()
                ->scalarNode('repository')->end()
                ->scalarNode('form')->defaultValue('Simply\LocationBundle\Form\Type\ModeReservationType')->end()
                ->end()
                ->end()
                ->arrayNode('Reservation')
                ->addDefaultsIfNotSet()
                ->children()
                ->scalarNode('model')->defaultValue('Simply\LocationBundle\Entity\Reservation')->end()
                ->scalarNode('controller')->defaultValue
                ('Sylius\Bundle\ResourceBundle\Controller\ResourceController')->end()
                ->scalarNode('repository')->end()
                ->scalarNode('form')->defaultValue('Simply\LocationBundle\Form\Type\Reservation')
                ->end()
                ->end()
                ->end()
                ->end()
                ->end()
                ->end();

            return $treeBuilder;
        }
    }