<?php

    namespace Simply\LocationBundle\DependencyInjection;

    use Sylius\Bundle\ResourceBundle\DependencyInjection\Extension\AbstractResourceExtension;
    use Symfony\Component\Config\FileLocator;
    use Symfony\Component\DependencyInjection\ContainerBuilder;
    use Symfony\Component\DependencyInjection\Loader;
    use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

    /**
     * This is the class that loads and manages your bundle configuration
     *
     * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
     */
    class LocationExtension extends AbstractResourceExtension
    {
        /**
         * {@inheritdoc}
         */
        public function load(array $configs, ContainerBuilder $container)
        {

            $loader = new YamlFileLoader(
                $container,
                new FileLocator(__DIR__ . '/../Resources/config')
            );

            $loader->load('services.yml');
        }
    }
