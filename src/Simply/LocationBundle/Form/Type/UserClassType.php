<?php

namespace Simply\LocationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserClassType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('login', 'text')
            ->add('mdp', 'text')
            ->add('nom', 'text')
            ->add('prenom', 'text')
            ->add('mail', 'text')
            ->add('tel', 'text')
            ->add('adresse', 'text')
            ->add('adresseComplement', 'text')
            ->add('matricule', 'text')
            ->add('civilite', 'text')
            ->add('mailPersonnel', 'text')
            ->add('telFixe', 'text')
            ->add('telPort', 'text')
            ->add('dateNaiss', 'date')
            ->add('group', 'entity', array(
                'class' => 'LocationBundle:GroupClass',
                'label' => 'Groupe de l\'utilisateur',
                'choice_label' => 'libelle',
                'multiple' => false,
                'expanded' => false
            ));


    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Simply\LocationBundle\Entity\UserClass'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'simply_bundles_locationbundle_userclass';
    }
}
