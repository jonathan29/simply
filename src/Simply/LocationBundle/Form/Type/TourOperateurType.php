<?php

namespace Simply\LocationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TourOperateurType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', 'text', array(
                'label' => 'location.touroperateur.form.label.nom'))
            ->add('mailContact', 'text', array(
                'label' => 'location.touroperateur.form.label.mail_contact'))
            ->add('mailDemande', 'text', array(
                'label' => 'location.touroperateur.form.label.mail_demande'))
            ->add('chqVacances', 'checkbox', array(
                'required' => false,
                'label' => 'location.touroperateur.form.label.chq_vacances'
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Simply\LocationBundle\Entity\TourOperateur'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'location_tour_operateur';
    }
}
