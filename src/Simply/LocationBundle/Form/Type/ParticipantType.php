<?php

namespace Simply\LocationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParticipantType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', 'text', array(
                'label'=>'nom',
                'attr'=>array(
                    'placeholder'=>'Nom du participant')))
            ->add('prenom', 'text', array(
                'label'=>'Prénom',
                'attr'=>array(
                    'placeholder'=>'Prénom du participant')))
            ->add('mail', 'text', array(
                'label'=>'Adresse email',
                'attr' => array(
                    'placeholder'=>'adresse email du participant')))
            ->add('dateNaiss','text', array(
                'label'=>'Date de naissance',
                'attr'=> array(
                    'placeholder' => 'dd-mm-yyyy',
                    'class'=> 'datepicker',
                    'data-provide'=>'datepicker',
                    'date-date-format'=>'dd-mm-yyyy')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Simply\LocationBundle\Entity\Participant'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'participant_type';
    }
}
