<?php

    namespace Simply\LocationBundle\Form\Type;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;

    class EnseigneType extends AbstractType
    {


        /**
         * @param FormBuilderInterface $builder
         * @param array                $options
         */
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
//            parent::buildForm($builder, $options);

            $builder
                ->add('nom', 'text', array(
                    'label' => 'Nom'))
                ->add('classement', 'number', array(
                    'label' => 'Classement'))
                ->add('description', 'textarea', array(
                    'label' => 'Description'))
                ->add('mail', 'text', array(
                    'label' => 'Mail de contact'))
                ->add('tel', 'text', array(
                    'label' => 'Téléphone'))
                ->add('latitude', 'text', array(
                    'label' => 'Latitude'))
                ->add('longitude', 'text', array(
                    'label' => 'Longitude'))
                ->add('debutSaison', 'date', array(
                    'label' => 'Début de la saison'))
                ->add('finSaison', 'date', array(
                    'label' => 'Fin de la saison'))
                ->add('adresse', 'text', array(
                    'label' => 'Adresse'))
                ->add('adresseComplement', 'text', array(
                    'label' => 'Complément d\'adresse'))
                ->add('images', new EnseigneImageType());

        }

        /**
         * @param OptionsResolverInterface $resolver
         */
        public function setDefaultOptions(OptionsResolverInterface $resolver)
        {
            $resolver->setDefaults(array(
                'data_class' => 'Simply\LocationBundle\Entity\Enseigne',
            ));
        }

        /**
         * @return string
         */
        public function getName()
        {
            return 'location_enseigne';
        }
    }
