<?php

namespace Simply\LocationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OffreLocationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebut', 'date', array(
                'label' => 'location.offrelocation.form.label.date_debut'))
            ->add('dateFin', 'date', array(
                'label' => 'location.offrelocation.form.label.date_fin'))
            ->add('tarif', 'text', array(
                'label' => 'location.offrelocation.form.label.tarif'))
            ->add('enabled', 'checkbox', array(
                'label' => 'location.offrelocation.form.label.enabled'))
            ->add('enseigne', 'entity', array(
                'label' => 'location.offrelocation.form.label.enseigne',
                'class' => 'LocationBundle:Enseigne',
                'choice_label' => 'nom',
                'multiple' => false,
                'expanded' => false,
            ))
            ->add('modeReservation', 'entity', array(
                'label' => 'location.offrelocation.form.label.mode_reservation',
                'class' => 'LocationBundle:ReservationMode',
                'choice_label' => 'libelle',
                'expanded' => false,
                'multiple' => false,
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Simply\LocationBundle\Entity\OffreLocation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'simply_bundles_locationbundle_offrelocation';
    }
}
