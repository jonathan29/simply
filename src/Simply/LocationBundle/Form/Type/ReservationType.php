<?php

    namespace Simply\LocationBundle\Form\Type;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\OptionsResolver;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;

    class ReservationType extends AbstractType
    {
        /**
         * @param FormBuilderInterface $builder
         * @param array                $options
         */
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
                ->remove('date')
                ->add('participants', 'collection', array(
                    'type'      => new ParticipantType(),
                    'allow_add' => TRUE,
                ));
        }

        /**
         * @param OptionsResolverInterface $resolver
         */
    /*    public function setDefaultOptions(OptionsResolverInterface $resolver)
        {
            $resolver->setDefaults(array(
                'data_class' => 'Simply\LocationBundle\Entity\Reservation',
            ));
        }*/

        public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
        {
            parent::configureOptions($resolver);
            $resolver->setDefaults(array(
                'data_class' => 'Simply\LocationBundle\Entity\Reservation',
            ));
        }

        /**
         * @return string
         */
        public function getName()
        {
            return 'simply_bundles_locationbundle_reservation';
        }
    }
