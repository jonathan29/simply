<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 05/11/2015
 * Time: 14:06
 */

namespace Simply\LocationBundle\Form\Type;

use Sylius\Bundle\CoreBundle\Form\Type\ProductTranslationType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;

class LocationTranslationType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('sizeAdvice', 'text', array(
                'required' => false,
                'label'    => 'Size advice'
            ));
    }
}