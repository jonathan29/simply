<?php

namespace Simply\LocationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HebergementFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelle', 'text', array('label' => 'Libelle'))
            ->add('description', 'textarea', array('label' => 'Description'))
            ->add('surface', 'text', array('label' => '.Surface'))
            ->add('nbChambre', 'text', array('label' => '.Nombre de chambres'))
            ->add('nbPiece', 'text', array('label' => '..Nombre de pièces'))
            ->add('nbMaxPlace', 'text', array('label' => 'Nombre de places Maximum'))
            ->add('nbPlace', 'text', array('label' => 'Nombre de places Minimum'))
            ->add('hebergementType', 'entity', array(
                'class' => 'LocationBundle:HebergementType',
                'label' => 'Type d\'hébergement',
                'choice_label' => 'libelle',
                'multiple' => false,
                'expanded' => false
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Simply\LocationBundle\Entity\Hebergement'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'simply_bundles_locationbundle_hebergement';
    }
}
