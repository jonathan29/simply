<?php
    /**
     * Created by PhpStorm.
     * User: jonathan
     * Date: 08/12/15
     * Time: 22:17
     */

    namespace Simply\LocationBundle\Form;


    use Simply\LocationBundle\Entity\EnseigneSearch;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\Form\OptionsResolver;


    class EnseigneSearchType extends AbstractType
    {
        protected $perPage = 10;
        protected $perPageChoices = array(10, 20, 50);

        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            // la liste de choix "perPage" est codée en dur. Dans un vrai projet, il ne faut pas faire ça
            $perPageChoices = array();
            foreach ($this->perPageChoices as $choice) {
                $perPageChoices[$choice] =  $choice . ' enseignes';
            }

            $builder
                ->add('nom', NULL, array(
                    'required' => FALSE,
                    'label'=> 'Recherche',
                    'attr'=> array(
                        'placeholder'=>'Votre Recherche')
                ))
                ->add('theme', 'entity', array(
                    'class'       => 'LocationBundle:EnseigneTheme',
                    'empty_data'  => NULL,
                    'label'=>'Thème',
                    'placeholder' => 'Thème',
                    'required'    => FALSE,
                    'attr'        => array(
                        'class' => 'cold-md-2',
                    ),
                ))
                ->add('search', 'submit', array(
                    'attr' => array(
                        'class' => 'btn btn-primary',
                    ),
                ))->add('sort', 'hidden', array(
                    'required' => FALSE,
                ))
                ->add('direction', 'hidden', array(
                    'required' => FALSE,
                ))
                ->add('sortSelect', 'choice', array(
                    'label'=>'Tri' ,
                    'choices'  => EnseigneSearch::$sortChoices,
                    'required' => FALSE,
                ))
                ->add('perPage', 'choice', array(
                    'label'=>'par page',
                    'choices'  => $perPageChoices,
                    'required' => FALSE,
                ))
                ->add('search', 'submit', array(
                    'label' => ' Rechercher',
                    'attr'  => array(
                        'class' => 'soap-icon-search full-width bounce',
                    ),
                ))
                ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                    // émule la soumission du sortSelect pour préremplir le champ
                    $enseigneSearch = $event->getData();

                    if (array_key_exists('sort', $enseigneSearch) && array_key_exists('direction', $enseigneSearch)) {
                        $enseigneSearch['sortSelect'] = $enseigneSearch['sort'] . ' ' . $enseigneSearch['direction'];
                    } else {
                        $enseigneSearch['sortSelect'] = '';
                    }
                    $event->setData($enseigneSearch);
                });
        }

        public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
        {
            parent::configureOptions($resolver);
            $resolver->setDefaults(array(
                'csrf_protection' => FALSE,
                'data_class'      => 'Simply\LocationBundle\Entity\EnseigneSearch',
            ));
        }

        public function getName()
        {
            return 'enseigne_search_type';
        }


    }