<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 30/11/2015
     * Time: 11:50
     */

    namespace Simply\LocationBundle\Entity;

    use Doctrine\ORM\EntityRepository;

    class ReservationRepository extends EntityRepository
    {
        //requete bons plans
        //requete notre selection
//    selection vacances petits budgets
        public function findUsersReservations(\Simply\UserBundle\Entity\User $user)
        {
            $qb = $this->_em->createQueryBuilder()->select('r ,s.libelle as etat,  e.nom as nom_enseigne, h.libelle
            as libelle_hebergement')
                ->from('LocationBundle:Reservation', 'r')
                ->join('r.offreLocation', 'o')
                ->join('o.location', 'l')
                ->join('l.enseigne', 'e')
                ->join('l.hebergement', 'h')
                ->join('r.etat', 's')
                ->where('r.user = :user')
                //etat est différent de archive
                ->andWhere('r.archive = 0 ')
                ->andWhere('s.statut < 2 ')
                ->setParameter('user', $user);;

            return $qb->getQuery()/*->getResult()*/
                ;
        }

        public function findReservationInfo($id)
        {
            $qb = $this->createQueryBuilder('r')
                ->join('r.offreLocation', 'o')
                ->join('o.location', 'l')
                ->join('l.enseigne', 'e')
                ->join('e.ville', 'v')
                ->join('l.hebergement', 'h')
                ->join('r.etat', 's')
                ->where('r.id = :id')
                ->setParameter('id', $id)
                ->addSelect('e.nom as nom_enseigne, e.description as
            description')
//            ->addSelect('e.nom as nom_enseigne')
                ->addSelect('l')
                ->addSelect('h')
                ->addSelect('o')
                ->addSelect('s ')
                ->addSelect('v.libelle as ville');

            return $qb->getQuery()->getSingleResult();

        }


    }