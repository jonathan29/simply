<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 12/11/2015
 * Time: 09:49
 */

namespace Simply\LocationBundle\Entity;


interface HebergementTypeInterface
{
    /**
     * Get Id
     *
     * @return int
     */
    public function getId();

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle();

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle);

}