<?php

namespace Simply\LocationBundle\Entity;

use FOS\UserBundle\FOSUserBundle as BaseUser;
/**
 * UserClass
 */
class UserClass extends BaseUser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $mdp;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var string
     */
    private $mail;

    /**
     * @var string
     */
    private $tel;

    /**
     * @var string
     */
    private $adresse;

    /**
     * @var string
     */
    private $adresseComplement;

    /**
     * @var string
     */
    private $matricule;

    /**
     * @var string
     */
    private $civilite;

    /**
     * @var string
     */
    private $mailPersonnel;

    /**
     * @var string
     */
    private $telFixe;

    /**
     * @var string
     */
    private $telPort;

    /**
     * @var string
     */
    private $dateNaiss;

    /**
     * @var
     */
     private $group;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return UserClass
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set mdp
     *
     * @param string $mdp
     *
     * @return UserClass
     */
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * Get mdp
     *
     * @return string
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return UserClass
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return UserClass
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return UserClass
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return UserClass
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return UserClass
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresseComplement
     *
     * @param string $adresseComplement
     *
     * @return UserClass
     */
    public function setAdresseComplement($adresseComplement)
    {
        $this->adresseComplement = $adresseComplement;

        return $this;
    }

    /**
     * Get adresseComplement
     *
     * @return string
     */
    public function getAdresseComplement()
    {
        return $this->adresseComplement;
    }

    /**
     * Set matricule
     *
     * @param string $matricule
     *
     * @return UserClass
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;

        return $this;
    }

    /**
     * Get matricule
     *
     * @return string
     */
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * Set civilite
     *
     * @param string $civilite
     *
     * @return UserClass
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set mailPersonnel
     *
     * @param string $mailPersonnel
     *
     * @return UserClass
     */
    public function setMailPersonnel($mailPersonnel)
    {
        $this->mailPersonnel = $mailPersonnel;

        return $this;
    }

    /**
     * Get mailPersonnel
     *
     * @return string
     */
    public function getMailPersonnel()
    {
        return $this->mailPersonnel;
    }

    /**
     * Set telFixe
     *
     * @param string $telFixe
     *
     * @return UserClass
     */
    public function setTelFixe($telFixe)
    {
        $this->telFixe = $telFixe;

        return $this;
    }

    /**
     * Get telFixe
     *
     * @return string
     */
    public function getTelFixe()
    {
        return $this->telFixe;
    }

    /**
     * Set telPort
     *
     * @param string $telPort
     *
     * @return UserClass
     */
    public function setTelPort($telPort)
    {
        $this->telPort = $telPort;

        return $this;
    }

    /**
     * Get telPort
     *
     * @return string
     */
    public function getTelPort()
    {
        return $this->telPort;
    }

    /**
     * Set dateNaiss
     *
     * @param string $dateNaiss
     *
     * @return UserClass
     */
    public function setDateNaiss( $dateNaiss)
    {
        $this->dateNaiss = $dateNaiss;

        return $this;
    }

    /**
     * Get dateNaiss
     *
     * @return string
     */
    public function getDateNaiss()
    {
        return $this->dateNaiss;
    }

    /**
     * Set group
     *
     * @param \Simply\LocationBundle\Entity\GroupClass $group
     *
     * @return UserClass
     */
    public function setGroup(\Simply\LocationBundle\Entity\GroupClass $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \Simply\LocationBundle\Entity\GroupClass
     */
    public function getGroup()
    {
        return $this->group;
    }
}
