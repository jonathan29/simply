<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 12/11/2015
     * Time: 09:30
     */

    namespace Simply\LocationBundle\Entity;


    use Doctrine\Common\Collections\ArrayCollection;

    class HebergementType
    {
        /**
         * Id
         *
         * @var integer
         */
        private $id;

        /**
         * libelle
         *
         * @var integer
         */
        private $libelle;

        /*
         * @var Image
         */
        private $image;


        public function __construct()
        {
            $this->hebergements = new ArrayCollection();
        }


        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @param mixed $id
         */
        public function setId($id)
        {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getLibelle()
        {
            return $this->libelle;
        }

        /**
         * @param mixed $libelle
         */
        public function setLibelle($libelle)
        {
            $this->libelle = $libelle;
        }


        /**
         * Set image
         *
         * @param \Simply\LocationBundle\Entity\HebergementTypeImage $image
         *
         * @return HebergementType
         */
        public function setImage($image)
//    public function setImage(\Simply\LocationBundle\Entity\HebergementTypeImage $image = null)
        {
            $this->image = $image;
        }

        /**
         * Get image
         *
         * @return \Simply\LocationBundle\Entity\HebergementTypeImage
         */
        public function getImage()
        {
            return $this->image;
        }

        function __toString()
        {
            return $this->libelle;
        }

    }
