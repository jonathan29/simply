<?php

namespace Simply\LocationBundle\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * HebergementTypeImage
 */
class HebergementTypeImage
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $alt;

    private $file;
    private $tmpFileName;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return HebergementTypeImage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set alt
     *
     * @param string $alt
     *
     * @return HebergementTypeImage
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    public function getUploadDir()
    {
        return 'uploads/img';
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../../web/' . $this->getUploadDir();
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
        if (null !== $this->url) {
            $this->tmpFileName = $this->url;

            $this->url = null;
            $this->alt = null;
        }
    }


    public function preUpload()
    {
        if (null == $this->file) {
            return;
        }
        $this->url = $this->file->guessExtension();
        $this->alt = $this->file->getClientOriginalName();
    }


    public function upload()
    {
        if (null == $this->file) {
            return;
        }
        if (null !== $this->tmpFileName) {
            $oldFile = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->tmpFileName;
            if (file_exists($oldFile)) {
                unlink($oldFile);
            }
        }
        $this->file->move($this->getUploadRootDir(),
            $this->id . '.' . $this->url);
    }

    public function preRemoveUpload()
    {
        // On sauvegarde temporairement le nom du fichier, car il    dépend de l'id
        $this->tmpFileName = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->url;
    }

    public function removeUpload()
    {
        // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
        if (file_exists($this->tmpFileName)) {
            // On supprime le fichier
            unlink($this->tmpFileName);
        }
    }

    public function getWebPath()
    {
        return $this->getUploadDir() . ' /' . $this->getId() . '.' . $this->getUrl();
    }

    function __toString()
    {
        return $this->alt;
    }

}
