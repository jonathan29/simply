<?php

    namespace Simply\LocationBundle\Entity;

    /**
     * Ville
     */
    class Ville
    {
        /**
         * @var integer
         */
        private $id;

        /**
         * @var string
         */
        private $libelle;
        /**
         * @var string
         */
        private $cp;

        /**
         * @var \Simply\LocationBundle\Entity\Departement
         */
        private $departement;
        /**
         * @var float
         */
        private $latitude;
        /**
         * @var float
         */
        private $longitude;


        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }


        /**
         * Set libelle
         *
         * @param string $libelle
         *
         * @return Ville
         */
        public function setLibelle($libelle)
        {
            $this->libelle = $libelle;

            return $this;
        }

        /**
         * Get libelle
         *
         * @return string
         */
        public function getLibelle()
        {
            return $this->libelle;
        }


        /**
         * Set cp
         *
         * @param string $cp
         *
         * @return Ville
         */
        public function setCp($cp)
        {
            $this->cp = $cp;

            return $this;
        }

        /**
         * Get cp
         *
         * @return string
         */
        public function getCp()
        {
            return $this->cp;
        }


        /**
         * Set latitude
         *
         * @param float $latitude
         *
         * @return Ville
         */
        public function setLatitude($latitude)
        {
            $this->latitude = $latitude;

            return $this;
        }

        /**
         * Get latitude
         *
         * @return float
         */
        public function getLatitude()
        {
            return $this->latitude;
        }

        /**
         * Set longitude
         *
         * @param float $longitude
         *
         * @return Ville
         */
        public function setLongitude($longitude)
        {
            $this->longitude = $longitude;

            return $this;
        }

        /**
         * Get longitude
         *
         * @return float
         */
        public function getLongitude()
        {
            return $this->longitude;
        }


        /**
         * Set departement
         *
         * @param \Simply\LocationBundle\Entity\Departement $departement
         *
         * @return Ville
         */
        public function setDepartement(\Simply\LocationBundle\Entity\Departement $departement = NULL)
        {
            $this->departement = $departement;

            return $this;
        }

        /**
         * Get departement
         *
         * @return \Simply\LocationBundle\Entity\Departement
         */
        public function getDepartement()
        {
            return $this->departement;
        }

        function __toString()
        {
            return $this->libelle;
        }

    }
