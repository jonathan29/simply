<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 28/12/2015
     * Time: 18:54
     */

    namespace Simply\LocationBundle\Entity;


    class ReglementMode
    {

        private $id;
        /**
         * @var string
         */
        private $libelle;


        /**
         * Set libelle
         *
         * @param string $libelle
         * @return ReglementMode
         */
        public function setLibelle($libelle)
        {
            $this->libelle = $libelle;

            return $this;
        }

        /**
         * Get libelle
         *
         * @return string
         */
        public function getLibelle()
        {
            return $this->libelle;
        }

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }
    }
