<?php

namespace Simply\LocationBundle\Entity;

/**
 * Region
 */
class Region
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var Simply\LocationBundle\Entity\Region

     */
    private $pays;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Region
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set pays
     *
     * @param \Simply\LocationBundle\Entity\Pays $pays
     *
     * @return Region
     */
    public function setPays(\Simply\LocationBundle\Entity\Pays $pays = null)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \Simply\LocationBundle\Entity\Pays
     */
    public function getPays()
    {
        return $this->pays;
    }
}
