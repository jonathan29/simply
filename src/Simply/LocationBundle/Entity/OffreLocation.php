<?php

    namespace Simply\LocationBundle\Entity;

    /**
     * OffreLocation
     */
    class OffreLocation
    {
        /**
         * @var integer
         */
        private $id;

        /**
         * @var \DateTime
         */
        private $dateDebut;

        /**
         * @var \DateTime
         */
        private $dateFin;

        /**
         * @var float
         */
        private $tarif;

        /**
         * @var boolean
         */
        private $enabled;


        private $dispo;


        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set dateDebut
         *
         * @param \DateTime $dateDebut
         *
         * @return OffreLocation
         */
        public function setDateDebut($dateDebut)
        {
            $this->dateDebut = $dateDebut;

            return $this;
        }

        /**
         * Get dateDebut
         *
         * @return \DateTime
         */
        public function getDateDebut()
        {
            return $this->dateDebut;
        }

        /**
         * Set dateFin
         *
         * @param \DateTime $dateFin
         *
         * @return OffreLocation
         */
        public function setDateFin($dateFin)
        {
            $this->dateFin = $dateFin;

            return $this;
        }

        /**
         * Get dateFin
         *
         * @return \DateTime
         */
        public function getDateFin()
        {
            return $this->dateFin;
        }

        /**
         * Set tarif
         *
         * @param float $tarif
         *
         * @return OffreLocation
         */
        public function setTarif($tarif)
        {
            $this->tarif = $tarif;

            return $this;
        }

        /**
         * Get tarif
         *
         * @return float
         */
        public function getTarif()
        {
            return $this->tarif;
        }

        /**
         * Set enabled
         *
         * @param boolean $enabled
         *
         * @return OffreLocation
         */
        public function setEnabled($enabled)
        {
            $this->enabled = $enabled;

            return $this;
        }

        /**
         * Get enabled
         *
         * @return boolean
         */
        public function getEnabled()
        {
            return $this->enabled;
        }


        /**
         * @var \Simply\LocationBundle\Entity\Location
         */
        private $location;


        /**
         * Set location
         *
         * @param \Simply\LocationBundle\Entity\Location $location
         *
         * @return OffreLocation
         */
        public function setLocation(\Simply\LocationBundle\Entity\Location $location = null)
        {
            $this->location = $location;

            return $this;
        }

        /**
         * Get location
         *
         * @return \Simply\LocationBundle\Entity\Location
         */
        public function getLocation()
        {
            return $this->location;
        }


        /**
         * Set dispo
         *
         * @param boolean $dispo
         *
         * @return OffreLocation
         */
        public function setDispo($dispo)
        {
            $this->dispo = $dispo;

            return $this;
        }

        /**
         * Get dispo
         *
         * @return boolean
         */
        public function getDispo()
        {
            return $this->dispo;
        }

        public function __toString()
        {
            return (String)$this->tarif;
        }
    }
