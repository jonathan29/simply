<?php

    namespace Simply\LocationBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * EnseigneTheme
     */
    class EnseigneTheme
    {
        /**
         * @var integer
         */
        private $id;

        /**
         * @var string
         */
        private $libelle;

        /**
         * @var boolean
         */
        private $etat;

        /**
         * @var \Doctrine\Common\Collections\Collection
         */
        private $enseignes;
        /**
         * @var \Simply\LocationBundle\Entity\EnseigneThemeImage
         */
        private $image;

        /**
         * Constructor
         */
        public function __construct()
        {
            $this->enseignes = new \Doctrine\Common\Collections\ArrayCollection();
        }

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set libelle
         *
         * @param string $libelle
         * @return EnseigneTheme
         */
        public function setLibelle($libelle)
        {
            $this->libelle = $libelle;

            return $this;
        }

        /**
         * Get libelle
         *
         * @return string
         */
        public function getLibelle()
        {
            return $this->libelle;
        }

        /**
         * Set etat
         *
         * @param boolean $etat
         * @return EnseigneTheme
         */
        public function setEtat($etat)
        {
            $this->etat = $etat;

            return $this;
        }

        /**
         * Get etat
         *
         * @return boolean
         */
        public function getEtat()
        {
            return $this->etat;
        }


        /**
         * Add enseignes
         *
         * @param \Simply\LocationBundle\Entity\Enseigne $enseignes
         * @return EnseigneTheme
         */
        public function addEnseigne(\Simply\LocationBundle\Entity\Enseigne $enseignes)
        {
            $this->enseignes[] = $enseignes;

            return $this;
        }

        /**
         * Remove enseignes
         *
         * @param \Simply\LocationBundle\Entity\Enseigne $enseignes
         */
        public function removeEnseigne(\Simply\LocationBundle\Entity\Enseigne $enseignes)
        {
            $this->enseignes->removeElement($enseignes);
        }

        /**
         * Get enseignes
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getEnseignes()
        {
            return $this->enseignes;
        }


        /**
         * Set image
         *
         * @param \Simply\LocationBundle\Entity\EnseigneThemeImage $image
         * @return EnseigneTheme
         */
        public function setImage($image)
//        public function setImage(\Simply\LocationBundle\Entity\EnseigneThemeImage $image = null)
        {
            $this->image = $image;

            return $this;
        }

        function __toString()
        {
            return $this->libelle;
        }


        /**
         * Get image
         *
         * @return \Simply\LocationBundle\Entity\EnseigneThemeImage
         */
        public function getImage()
        {
            return $this->image;
        }
    }
