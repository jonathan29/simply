<?php

    namespace Simply\LocationBundle\Entity;

    /**
     * Etat
     */
    class Etat
    {
        /**
         * @var integer
         */
        private $id;

        /**
         * @var string
         */
        private $libelle;

        /**
         * @var integer
         */
        private $statut;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set libelle
         *
         * @param string $libelle
         *
         * @return Etat
         */
        public function setLibelle($libelle)
        {
            $this->libelle = $libelle;

            return $this;
        }

        /**
         * Get libelle
         *
         * @return string
         */
        public function getLibelle()
        {
            return $this->libelle;
        }

        public function __toString()
        {
            return $this->libelle;
        }

        /**
         * Set statut
         *
         * @param integer $statut
         * @return Etat
         */
        public function setStatut($statut)
        {
            $this->statut = $statut;

            return $this;
        }

        /**
         * Get statut
         *
         * @return integer
         */
        public function getStatut()
        {
            return $this->statut;
        }
    }
