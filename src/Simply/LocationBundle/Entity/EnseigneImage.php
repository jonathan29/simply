<?php

    namespace Simply\LocationBundle\Entity;

    /**
     * HebergementTypeImage
     */
    class EnseigneImage extends Image
    {
        /**
         * @var integer
         */
        protected $id;

        

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        function __toString()
        {
//            return $this->alt;
            return 'enseigne image';
        }


}
