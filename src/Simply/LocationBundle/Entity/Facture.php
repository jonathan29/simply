<?php

namespace Simply\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Facture
 */
class Facture
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $numFacture;

    /**
     * @var \DateTime
     */
    private $date;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numFacture
     *
     * @param string $numFacture
     * @return Facture
     */
    public function setNumFacture($numFacture)
    {
        $this->numFacture = $numFacture;

        return $this;
    }

    /**
     * Get numFacture
     *
     * @return string 
     */
    public function getNumFacture()
    {
        return $this->numFacture;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Facture
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * @var \Simply\UserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \Simply\UserBundle\Entity\User $user
     * @return Facture
     */
    public function setUser(\Simply\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Simply\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \Simply\LocationBundle\Entity\Reglement
     */
    private $reglements;


    /**
     * Set reglements
     *
     * @param \Simply\LocationBundle\Entity\Reglement $reglements
     * @return Facture
     */
    public function setReglements(\Simply\LocationBundle\Entity\Reglement $reglements = null)
    {
        $this->reglements = $reglements;

        return $this;
    }

    /**
     * Get reglements
     *
     * @return \Simply\LocationBundle\Entity\Reglement
     */
    public function getReglements()
    {
        return $this->reglements;
    }
    /**
     * @var \Simply\LocationBundle\Entity\Reservation
     */
    private $reservation;


    /**
     * Set reservation
     *
     * @param \Simply\LocationBundle\Entity\Reservation $reservation
     * @return Facture
     */
    public function setReservation(\Simply\LocationBundle\Entity\Reservation $reservation)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \Simply\LocationBundle\Entity\Reservation
     */
    public function getReservation()
    {
        return $this->reservation;
    }
}
