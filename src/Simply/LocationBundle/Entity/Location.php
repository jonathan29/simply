<?php

namespace Simply\LocationBundle\Entity;

/**
 * Location
 */
class Location
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $etat;

    /**
     * @var Hebergement
     */
    private $hebergement;

    /**
     * @var TourOperateur
     */
    private $tourOperateur;

    /**
     * @var Enseigne
     */
    private $enseigne;

    /**
     * @var ReservationMode
     */
    private $modeReservation;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $offresLocation;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->offresLocation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->etat = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     *
     * @return Location
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set hebergement
     *
     * @param \Simply\LocationBundle\Entity\Hebergement $hebergement
     *
     * @return Location
     */
    public function setHebergement(\Simply\LocationBundle\Entity\Hebergement $hebergement = null)
    {
        $this->hebergement = $hebergement;

        return $this;
    }

    /**
     * Get hebergement
     *
     * @return \Simply\LocationBundle\Entity\Hebergement
     */
    public function getHebergement()
    {
        return $this->hebergement;
    }

    /**
     * Set tourOperateur
     *
     * @param \Simply\LocationBundle\Entity\TourOperateur $tourOperateur
     *
     * @return Location
     */
    public function setTourOperateur(\Simply\LocationBundle\Entity\TourOperateur $tourOperateur = null)
    {
        $this->tourOperateur = $tourOperateur;

        return $this;
    }

    /**
     * Get tourOperateur
     *
     * @return \Simply\LocationBundle\Entity\TourOperateur
     */
    public function getTourOperateur()
    {
        return $this->tourOperateur;
    }

    /**
     * Set enseigne
     *
     * @param \Simply\LocationBundle\Entity\Enseigne $enseigne
     *
     * @return Location
     */
    public function setEnseigne(\Simply\LocationBundle\Entity\Enseigne $enseigne = null)
    {
        $this->enseigne = $enseigne;

        return $this;
    }

    /**
     * Get enseigne
     *
     * @return \Simply\LocationBundle\Entity\Enseigne
     */
    public function getEnseigne()
    {
        return $this->enseigne;
    }

    /**
     * Add offresLocation
     *
     * @param \Simply\LocationBundle\Entity\OffreLocation $offresLocation
     *
     * @return Location
     */
    public function addOffresLocation(\Simply\LocationBundle\Entity\OffreLocation $offresLocation)
    {
        $this->offresLocation[] = $offresLocation;

        return $this;
    }

    /**
     * Remove offresLocation
     *
     * @param \Simply\LocationBundle\Entity\OffreLocation $offresLocation
     */
    public function removeOffresLocation(\Simply\LocationBundle\Entity\OffreLocation $offresLocation)
    {
        $this->offresLocation->removeElement($offresLocation);
    }

    /**
     * Get offresLocation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffresLocation()
    {
        return $this->offresLocation;
    }

   public function __toString()
    {
        return 'location';
    }

    /**
     * Set modeReservation
     *
     * @param \Simply\LocationBundle\Entity\ReservationMode $modeReservation
     *
     * @return Location
     */
    public function setModeReservation(\Simply\LocationBundle\Entity\ReservationMode $modeReservation = null)
    {
        $this->modeReservation = $modeReservation;

        return $this;
    }

    /**
     * Get modeReservation
     *
     * @return \Simply\LocationBundle\Entity\ReservationMode
     */
    public function getModeReservation()
    {
        return $this->modeReservation;
    }
}
