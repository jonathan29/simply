<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 28/12/2015
     * Time: 19:00
     */

    namespace Simply\LocationBundle\Entity;


    class Reglement
    {

        /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float
     */
    private $montant;

    /**
     * @var boolean
     */
    private $confirmation;

    /**
     * @var boolean
     */
    private $accompte;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Simply\LocationBundle\Entity\ReglementMode
     */
    private $mode;


    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Reglement
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set montant
     *
     * @param float $montant
     * @return Reglement
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float 
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set confirmation
     *
     * @param boolean $confirmation
     * @return Reglement
     */
    public function setConfirmation($confirmation)
    {
        $this->confirmation = $confirmation;

        return $this;
    }

    /**
     * Get confirmation
     *
     * @return boolean 
     */
    public function getConfirmation()
    {
        return $this->confirmation;
    }

    /**
     * Set accompte
     *
     * @param boolean $accompte
     * @return Reglement
     */
    public function setAccompte($accompte)
    {
        $this->accompte = $accompte;

        return $this;
    }

    /**
     * Get accompte
     *
     * @return boolean 
     */
    public function getAccompte()
    {
        return $this->accompte;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mode
     *
     * @param \Simply\LocationBundle\Entity\ReglementMode $mode
     * @return Reglement
     */
    public function setMode(\Simply\LocationBundle\Entity\ReglementMode $mode = null)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return \Simply\LocationBundle\Entity\ReglementMode
     */
    public function getMode()
    {
        return $this->mode;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $facture;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->facture = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add facture
     *
     * @param \Simply\LocationBundle\Entity\Facture $facture
     * @return Reglement
     */
    public function addFacture(\Simply\LocationBundle\Entity\Facture $facture)
    {
        $this->facture[] = $facture;

        return $this;
    }

    /**
     * Remove facture
     *
     * @param \Simply\LocationBundle\Entity\Facture $facture
     */
    public function removeFacture(\Simply\LocationBundle\Entity\Facture $facture)
    {
        $this->facture->removeElement($facture);
    }

    /**
     * Get facture
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFacture()
    {
        return $this->facture;
    }
}
