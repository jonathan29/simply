<?php

namespace Simply\LocationBundle\Entity;

/**
 * Departement
 */
class Departement
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $numero;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var /Simply\LocationBundle\Entity\Region
     */
    private $region;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Departement
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Departement
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set region
     *
     * @param \Simply\LocationBundle\Entity\Region $region
     *
     * @return Departement
     */
    public function setRegion(\Simply\LocationBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Simply\LocationBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $villes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->villes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add villes
     *
     * @param \Simply\LocationBundle\Entity\ville $villes
     * @return Departement
     */
    public function addVille(\Simply\LocationBundle\Entity\ville $villes)
    {
        $this->villes[] = $villes;

        return $this;
    }

    /**
     * Remove villes
     *
     * @param \Simply\LocationBundle\Entity\ville $villes
     */
    public function removeVille(\Simply\LocationBundle\Entity\ville $villes)
    {
        $this->villes->removeElement($villes);
    }

    /**
     * Get villes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVilles()
    {
        return $this->villes;
    }
}
