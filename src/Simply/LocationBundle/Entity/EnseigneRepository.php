<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 01/12/2015
     * Time: 13:50
     */

    namespace Simply\LocationBundle\Entity;

    use Doctrine\ORM\EntityRepository;
    use Simply\LocationBundle\Entity\Enseigne;

    class EnseigneRepository extends EntityRepository
    {
        public function findEnseigneReservation($idOffre)
        {
            $qb = $this->_em->createQueryBuilder()
                ->select('e')
                ->from('LocationBundle:Enseigne', 'e')
                ->join('e.locations', 'l')
                ->join('l.offresLocation', 'o')
                ->where('o.id = :idOffre')
                ->setParameter('idOffre', $idOffre);

            return $qb->getQuery()->getSingleResult();
        }

        public function findMinPrix(Enseigne $enseigne){
            $qb = $this->_em->createQueryBuilder()
                ->select('Min(o.tarif) as tarifmini')
                ->from('LocationBundle:Enseigne', 'e')
                ->join('e.locations', 'l')
                ->join('l.offresLocation', 'o')
                ->where('e.id = :idOffre')
                ->setParameter('idOffre', $enseigne->getId());

            return $qb->getQuery()->getSingleResult();

        }
    }
