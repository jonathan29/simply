<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 01/12/2015
     * Time: 13:50
     */

    namespace Simply\LocationBundle\Entity;

    use Doctrine\ORM\EntityRepository;

    class LocationRepository extends EntityRepository
    {
        public function findPrixMini($limit = NULL)
        {
            $qb = $this->_em->createQueryBuilder()
                ->select(' l ,
                    e.id as enseigne_id, e.nom as enseigne, e.classement,
                    v.libelle as nomVille,
                    MIN(o.tarif) as tarifmini',
                    'IDENTITY(e.image) as image_id')
                ->from('LocationBundle:Location', 'l')
                ->join('l.enseigne', 'e')
                ->join('l.hebergement', 'h')
                ->join('l.offresLocation', 'o')
                ->join('e.ville', 'v')
                ->groupBy('e')
                ->where('e.etat = true')
                ->orderBy('tarifmini', 'ASC');

            if ($limit != NULL) {
                $qb->setMaxResults($limit);
            }
            return $qb->getQuery()->getResult();
//            $query = $qb->getQuery();
//            $query->useQueryCache(TRUE);
//            $query->useResultCache(TRUE);
//            $query->setResultCacheLifetime(5);
//
//            return $query->getResult();
        }

        public function findlast($limit = NULL)
        {
            $qb = $this->_em->createQueryBuilder()
                ->select(' l , e.id as enseigne_id, e.nom as enseigne, e.classement, v.libelle as nomVille, MIN(o
            .tarif) as tarifmini', 'IDENTITY(e.image) as image_id')
                ->from('LocationBundle:Location', 'l')
                ->join('l.enseigne', 'e')
                ->join('l.hebergement', 'h')
                ->join('l.offresLocation', 'o')
                ->join('e.ville', 'v')
                ->where('e.etat = true')
                ->groupBy('l')
                ->orderBy('e.id', 'DESC');

            if ($limit != NULL) {
                $qb->setMaxResults($limit);
            }

            return $qb->getQuery()->getResult();
//            $query = $qb->getQuery();
//            $query->useQueryCache(TRUE);
//            $query->useResultCache(TRUE);
//            $query->setResultCacheLifetime(5);
//
//            return $query->getResult();
        }

    }
