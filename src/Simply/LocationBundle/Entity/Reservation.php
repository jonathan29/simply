<?php

    namespace Simply\LocationBundle\Entity;

    /**
     * Reservation
     */
    class Reservation
    {
        /**
         * @var integer
         */
        private $id;

        /**
         * @var \DateTime
         */
        private $date;

        /**
         * @var \Simply\LocationBundle\Entity\Etat
         */
        private $etat;

        /**
         * @var \Simply\UsernBundle\Entity\User
         */
        private $user;

        /**
         *  \Simply\LocationBundle\Entity\Collection
         */
        private $participants;


        /**
         * Constructor
         */
        public function __construct()
        {
            $this->participants = new \Doctrine\Common\Collections\ArrayCollection();
            $this->date = new \DateTime();
            $this->archive = FALSE;
        }

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set date
         *
         * @param \DateTime $date
         *
         * @return Reservation
         */
        public function setDate($date)
        {
            $this->date = $date;

            return $this;
        }

        /**
         * Get date
         *
         * @return \DateTime
         */
        public function getDate()
        {
            return $this->date;
        }

        /**
         * Add participant
         *
         * @param \Simply\LocationBundle\Entity\Participant $participant
         *
         * @return Reservation
         */
        public function addParticipant(\Simply\LocationBundle\Entity\Participant $participant)
        {
            $this->participants[] = $participant;

            return $this;
        }

        /**
         * Remove participant
         *
         * @param \Simply\LocationBundle\Entity\Participant $participant
         */
        public function removeParticipant(\Simply\LocationBundle\Entity\Participant $participant)
        {
            $this->participants->removeElement($participant);
        }

        /**
         * Get participants
         *
         * @return \Doctrine\Common\Collections\Collection
         */
        public function getParticipants()
        {
            return $this->participants;
        }

        /**
         * Set user
         *
         * @param \Simply\UserBundle\Entity\User $user
         *
         * @return Reservation
         */
        public function setUser(\Simply\UserBundle\Entity\User $user = NULL)
        {
            $this->user = $user;

            return $this;
        }

        /**
         * Get user
         *
         * @return \Simply\UserBundle\Entity\User
         */
        public function getUser()
        {
            return $this->user;
        }


        /**
         * Set etat
         *
         * @param \Simply\LocationBundle\Entity\Etat $etat
         *
         * @return Reservation
         */
        public function setEtat(\Simply\LocationBundle\Entity\Etat $etat = NULL)
        {
            $this->etat = $etat;

            return $this;
        }

        /**
         * Get etat
         *
         * @return \Simply\LocationBundle\Entity\Etat
         */
        public function getEtat()
        {
            return $this->etat;
        }

        /**
         * @var \Simply\LocationBundle\Entity\OffreLocation
         */
        private $offreLocation;


        /**
         * Set offreLocation
         *
         * @param \Simply\LocationBundle\Entity\OffreLocation $offreLocation
         *
         * @return Reservation
         */
        public function setOffreLocation(\Simply\LocationBundle\Entity\OffreLocation $offreLocation = NULL)
        {
            $this->offreLocation = $offreLocation;

            return $this;
        }

        /**
         * Get offreLocation
         *
         * @return \Simply\LocationBundle\Entity\OffreLocation
         */
        public function getOffreLocation()
        {
            return $this->offreLocation;
        }

        /**
         * @var boolean
         */
        private $archive = '
                    false
                ';


        /**
         * Set archive
         *
         * @param boolean $archive
         * @return Reservation
         */
        public function setArchive($archive)
        {
            $this->archive = $archive;

            return $this;
        }

        /**
         * Get archive
         *
         * @return boolean
         */
        public function getArchive()
        {
            return $this->archive;
        }
    }
