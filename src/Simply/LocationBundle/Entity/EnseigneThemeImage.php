<?php

    namespace Simply\LocationBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * EnseigneThemeImage
     */
    class EnseigneThemeImage extends Image
    {
        /**
         * @var integer
         */
        protected $id;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

    }
