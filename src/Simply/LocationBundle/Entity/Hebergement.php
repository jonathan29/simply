<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 12/11/2015
     * Time: 11:56
     */

    namespace Simply\LocationBundle\Entity;


    class Hebergement
    {

        protected $id;
        protected $libelle;
        protected $description;
        protected $surface;
        protected $nbChambre;
        protected $nbPiece;
        protected $nbMaxPlace;
        protected $nbPlace;


        protected $hebergementType;

        /**
         * @return mixed
         */
        public function getHebergementType()
        {
            return $this->hebergementType;
        }

        /**
         * @param mixed $hebergementType
         */
        public function setHebergementType(HebergementType $hebergementType)
        {
            $this->hebergementType = $hebergementType;
        }

        /**
         * @return mixed
         */
        public function getDescription()
        {
            return $this->description;
        }

        /**
         * @param mixed $description
         */
        public function setDescription($description)
        {
            $this->description = $description;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @return mixed
         */
        public function getLibelle()
        {
            return $this->libelle;
        }

        /**
         * @param mixed $libelle
         */
        public function setLibelle($libelle)
        {
            $this->libelle = $libelle;
        }

        /**
         * @return mixed
         */
        public function getNbChambre()
        {
            return $this->nbChambre;
        }

        /**
         * @param mixed $nbChambre
         */
        public function setNbChambre($nbChambre)
        {
            $this->nbChambre = $nbChambre;
        }

        /**
         * @return mixed
         */
        public function getNbMaxPlace()
        {
            return $this->nbMaxPlace;
        }

        /**
         * @param mixed $nbMaxPlace
         */
        public function setNbMaxPlace($nbMaxPlace)
        {
            $this->nbMaxPlace = $nbMaxPlace;
        }

        /**
         * @return mixed
         */
        public function getNbPiece()
        {
            return $this->nbPiece;
        }

        /**
         * @param mixed $nbPiece
         */
        public function setNbPiece($nbPiece)
        {
            $this->nbPiece = $nbPiece;
        }

        /**
         * @return mixed
         */
        public function getNbPlace()
        {
            return $this->nbPlace;
        }

        /**
         * @param mixed $nbPlace
         */
        public function setNbPlace($nbPlace)
        {
            $this->nbPlace = $nbPlace;
        }

        /**
         * @return mixed
         */
        public function getSurface()
        {
            return $this->surface;
        }

        /**
         * @param mixed $surface
         */
        public function setSurface($surface)
        {
            $this->surface = $surface;
        }

        function __toString()
        {
            return $this->libelle;
        }
    }
