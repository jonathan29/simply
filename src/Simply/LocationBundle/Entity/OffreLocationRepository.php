<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 30/11/2015
     * Time: 11:50
     */

    namespace Simply\LocationBundle\Entity;

    use Doctrine\ORM\EntityRepository;

    class OffreLocationRepository extends EntityRepository
    {
        public function findMoinsCherByEnseigne(Enseigne $enseigne)
        {
            $qb = $this->_em->createQueryBuilder()
                ->select('Min(o.tarif)')
                ->from('LocationBundle:OffreLocation', 'o')
                ->join('o.location', 'l')
                ->join('l.enseigne', 'e')
                ->where('e = :enseigne')
                ->setParameter('enseigne', $enseigne);

            return $qb->getQuery()->getSingleScalarResult();
        }

        public function findMoinsCher()
        {
            $qb = $this->_em->createQueryBuilder()
                ->select('o')
                ->addSelect('min(o.tarif)as test')
                ->from('LocationBundle:OffreLocation', 'o')
                ->join('o.location', 'l')
                ->join('l.enseigne','e')
                ->where('e.etat = true')
                ->groupBy('e')
                ->orderBy('test', 'ASC');

            return $qb->getQuery()->getResult();
        }
    }