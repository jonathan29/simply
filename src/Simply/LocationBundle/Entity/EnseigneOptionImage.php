<?php

    namespace Simply\LocationBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * EnseigneOptionImage
     */
    class EnseigneOptionImage extends Image
    {
        /**
         * @var integer
         */
        protected $id;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        public function __toString()
        {
            return 'enseigne option image';
        }
    }
