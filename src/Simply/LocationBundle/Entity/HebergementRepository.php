<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 01/12/2015
     * Time: 13:50
     */

    namespace Simply\LocationBundle\Entity;

    use Doctrine\ORM\EntityRepository;

    class HebergementRepository extends EntityRepository
    {
        public function findHebergementReservation($idOffre)
        {
            $qb = $this->_em->createQueryBuilder()
                ->select('h')
                ->from('LocationBundle:Hebergement', 'h')
                ->join('h.locations', 'l')
                ->join('l.offresLocation', 'o')
                ->where('o.id = :idOffre')
                ->setParameter('idOffre', $idOffre);

            return $qb->getQuery()->getSingleResult();
        }
    }
