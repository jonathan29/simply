<?php
    /**
     * Created by PhpStorm.
     * User: jonathan
     * Date: 08/12/15
     * Time: 22:35
     */

    namespace Simply\LocationBundle\Entity\SearchRepository;


    use Elastica\Query;
    use Elastica\Query\MatchAll;
    use FOS\ElasticaBundle\Repository;
    use Simply\LocationBundle\Entity\EnseigneSearch;

    class EnseigneRepository extends Repository
    {
        public function search(EnseigneSearch $enseigneSearch)
        {
            if ($enseigneSearch->getNom() != NULL && $enseigneSearch != '') {
                $q = new Query();
                $multiMatchQuery = new Query\MultiMatch();
                $multiMatchQuery->setQuery($enseigneSearch->getNom());
                $multiMatchQuery->setFields(array(
                    'nom^10',
                    'ville.libelle^4',
                    'ville.departement.libelle^5',
                    'ville.departement.region.libelle',
                    'ville.departement.region.pays.libelle',
                ));
                $multiMatchQuery->setMinimumShouldMatch("60%");
                $q->setQuery($multiMatchQuery);
                if ($enseigneSearch->getTheme() != NULL) {
                    $filter = new \Elastica\Filter\Bool();
                    $term = new \Elastica\Filter\Term();
                    $term->setTerm('theme.libelle', $enseigneSearch->getTheme()->getLibelle());
                    $filter->addMust($term);
                    $filtered = new \Elastica\Query\Filtered($multiMatchQuery, $filter);

                    $query = \Elastica\Query::create($filtered);
                } else {
                    $query = $q;
                }
                // $query->setSort(
                //     array(
                //         $enseigneSearch->getSort() => (array(
                //             'order' => $enseigneSearch->getDirection(),
                //         )),
                //     ));
            } else {
                $query = new MatchAll();
            }
            return $this->find($query,20);
        }
    }