<?php

namespace Simply\LocationBundle\Entity;

use JMS\Serializer\Annotation as Serializer;
use FOS\ElasticaBundle\Configuration\Search;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Serializer\XmlRoot("ens")
 * @Search(repositoryClass="Simply\LocationBundle\Entity\SearchRepository\EnseigneRepository")
 * @ORM\HasLifecycleCallbacks
 * Enseigne
 */
class Enseigne{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $nom;
    /**
     * @var integer
     */
    private $classement;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $mail;
    /**
     *
     * @var string
     */
    private $tel;
    /**
     * @var float
     */
    private $latitude;
    /**
     * @var float
     */
    private $longitude;
    /**
     * @var \DateTime
     */
    private $debutSaison;
    /**
     * @var \DateTime
     */
    private $finSaison;
    /**
     * @var String
     */
    private $adresse;
    /**
     * @var String
     */
    private $adresseComplement;
    /**
     * @var Boolean
     */
    private $etat;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $locations;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ville;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $pension;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $options;
    /**
     * @var \Simply\LocationBundle\Entity\EnseigneTheme
     */
    private $theme;
    /**
     * @var \Simply\LocationBundle\Entity\EnseigneImage
     */
    private $image;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Enseigne
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set classement
     *
     * @param integer $classement
     *
     * @return Enseigne
     */
    public function setClassement($classement)
    {
        $this->classement = $classement;

        return $this;
    }

    /**
     * Get classement
     *
     * @return integer
     */
    public function getClassement()
    {
        return $this->classement;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Enseigne
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Enseigne
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return Enseigne
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Enseigne
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Enseigne
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     *
     * /**
     * Set debutSaison
     *
     * @param \DateTime $debutSaison
     *
     * @return Enseigne
     */
    public function setDebutSaison($debutSaison)
    {
        $this->debutSaison = $debutSaison;

        return $this;
    }

    /**
     * Get debutSaison
     *
     * @return \DateTime
     */
    public function getDebutSaison()
    {
        return $this->debutSaison;
    }

    /**
     * Set finSaison
     *
     * @param \DateTime $finSaison
     *
     * @return Enseigne
     */
    public function setFinSaison($finSaison)
    {
        $this->finSaison = $finSaison;

        return $this;
    }

    /**
     * Get finSaison
     *
     * @return \DateTime
     */
    public function getFinSaison()
    {
        return $this->finSaison;
    }

    /**
     * Set adresse
     *
     * @param \varchar $adresse
     *
     * @return Enseigne
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return \varchar
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresseComplement
     *
     * @param \varchar $adresseComplement
     *
     * @return Enseigne
     */
    public function setAdresseComplement($adresseComplement)
    {
        $this->adresseComplement = $adresseComplement;

        return $this;
    }

    /**
     * Get adresseComplement
     *
     * @return \varchar
     */
    public function getAdresseComplement()
    {
        return $this->adresseComplement;
    }

    function __toString()
    {
        return $this->nom;
    }

    /**
     * Add location
     *
     * @param \Simply\LocationBundle\Entity\Location $location
     *
     * @return Enseigne
     */
    public function addLocation(\Simply\LocationBundle\Entity\Location $location)
    {
        $this->locations[] = $location;

        return $this;
    }

    /**
     * Remove location
     *
     * @param \Simply\LocationBundle\Entity\Location $location
     */
    public function removeLocation(\Simply\LocationBundle\Entity\Location $location)
    {
        $this->locations->removeElement($location);
    }

    /**
     * Get locations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocations()
    {
        return $this->locations;
    }


    /**
     * Set etat
     *
     * @param boolean $etat
     *
     * @return Enseigne
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $option;


    /**
     * Set ville
     *
     * @param \Simply\LocationBundle\Entity\Ville $ville
     *
     * @return Enseigne
     */
    public function setVille(\Simply\LocationBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \Simply\LocationBundle\Entity\Ville
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pension
     *
     * @param \Simply\LocationBundle\Entity\Pension $pension
     *
     * @return Enseigne
     */
    public function setPension(\Simply\LocationBundle\Entity\Pension $pension = null)
    {
        $this->pension = $pension;

        return $this;
    }

    /**
     * Get pension
     *
     * @return \Simply\LocationBundle\Entity\Pension
     */
    public function getPension()
    {
        return $this->pension;
    }

    /**
     * Add option
     *
     * @param \Simply\LocationBundle\Entity\EnseigneOptionS $option
     *
     * @return Enseigne
     */
    public function addOption(\Simply\LocationBundle\Entity\EnseigneOption $option)
    {
        $this->option[] = $option;

        return $this;
    }

    /**
     * Remove option*
     * @param \Simply\LocationBundle\Entity\EnseigneOptionS $option
     */
    public function removeOption(\Simply\LocationBundle\Entity\EnseigneOption $option)
    {
        $this->option->removeElement($option);
    }

    /**
     * Get option
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set image
     *
     * @param \Simply\LocationBundle\Entity\EnseigneImage $image
     *
     * @return Enseigne
     */
    public function setImage($image = null)
//        public function setImage(\Simply\LocationBundle\Entity\EnseigneImage $image = null)
    {
        $this->image = $image;

        return $this;
    }


    /**
     * Set theme
     *
     * @param \Simply\LocationBundle\Entity\EnseigneTheme $theme
     * @return Enseigne
     */
    public function setTheme(\Simply\LocationBundle\Entity\EnseigneTheme $theme = null)
    {
        $this->theme = $theme;
    }

    /**
     * Get theme
     * @return \Simply\LocationBundle\Entity\EnseigneTheme
     */
    public function getTheme()
    {
        return $this->theme;
    }


    /**
     * Get image
     *
     * @return \Simply\LocationBundle\Entity\EnseigneImage
     */
    public function getImage()
    {
        return $this->image;
    }
}
