<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 12/11/2015
 * Time: 12:01
 */

namespace Simply\LocationBundle\Entity;


interface HebergementInterface
{
    public function getDescription();

    /**
     * @param mixed $description
     */
    public function setDescription($description);

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getLibelle();

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle);

    /**
     * @return mixed
     */
    public function getNbChambre();


    /**
     * @param mixed $nbChambre
     */
    public function setNbChambre($nbChambre);

    /**
     * @return mixed
     */
    public function getNbMaxPlace();

    /**
     * @param mixed $nbMaxPlace
     */
    public function setNbMaxPlace($nbMaxPlace);

    /**
     * @return mixed
     */
    public function getNbPiece();


    /**
     * @param mixed $nbPiece
     */
    public function setNbPiece($nbPiece);


    /**
     * @return mixed
     */
    public function getNbPlace();


    /**
     * @param mixed $nbPlace
     */
    public function setNbPlace($nbPlace);


    /**
     * @return mixed
     */
    public function getSurface();

    /**
     * @param mixed $surface
     */
    public function setSurface($surface);

}