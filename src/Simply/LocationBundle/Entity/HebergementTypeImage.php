<?php

    namespace Simply\LocationBundle\Entity;

    /**
     * HebergementTypeImage
     */
    class HebergementTypeImage extends Image
    {
        /**
         * @var integer
         */
        protected $id;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        public function __toString()
        {
            return $this->alt;
        }
    }
