<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 28/12/2015
     * Time: 20:01
     */

    namespace Simply\LocationBundle\Entity;


    class Message
    {

        /**
     * @var string
     */
    private $sujet;

    /**
     * @var string
     */
    private $message;

    /**
     * @var boolean
     */
    private $system;

    /**
     * @var \DateTime
     */
    private $dateEnvoi;

    /**
     * @var boolean
     */
    private $lu;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Simply\UserBundle\Entity\User
     */
    private $expediteur;

    /**
     * @var \Simply\UserBundle\Entity\User
     */
    private $destinataire;

    /**
     * @var \Simply\LocationBundle\Entity\Reservation
     */
    private $reservation;


    /**
     * Set sujet
     *
     * @param string $sujet
     * @return Message
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;

        return $this;
    }

    /**
     * Get sujet
     *
     * @return string 
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set system
     *
     * @param boolean $system
     * @return Message
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    /**
     * Get system
     *
     * @return boolean 
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Set dateEnvoi
     *
     * @param \DateTime $dateEnvoi
     * @return Message
     */
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;

        return $this;
    }

    /**
     * Get dateEnvoi
     *
     * @return \DateTime 
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * Set lu
     *
     * @param boolean $lu
     * @return Message
     */
    public function setLu($lu)
    {
        $this->lu = $lu;

        return $this;
    }

    /**
     * Get lu
     *
     * @return boolean 
     */
    public function getLu()
    {
        return $this->lu;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Message
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expediteur
     *
     * @param \Simply\UserBundle\Entity\User $expediteur
     * @return Message
     */
    public function setExpediteur(\Simply\UserBundle\Entity\User $expediteur = null)
    {
        $this->expediteur = $expediteur;

        return $this;
    }

    /**
     * Get expediteur
     *
     * @return \Simply\UserBundle\Entity\User
     */
    public function getExpediteur()
    {
        return $this->expediteur;
    }

    /**
     * Set destinataire
     *
     * @param \Simply\UserBundle\Entity\User $destinataire
     * @return Message
     */
    public function setDestinataire(\Simply\UserBundle\Entity\User $destinataire = null)
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    /**
     * Get destinataire
     *
     * @return \Simply\UserBundle\Entity\User
     */
    public function getDestinataire()
    {
        return $this->destinataire;
    }

    /**
     * Set reservation
     *
     * @param \Simply\LocationBundle\Entity\Reservation $reservation
     * @return Message
     */
    public function setReservation(\Simply\LocationBundle\Entity\Reservation $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \Simply\LocationBundle\Entity\Reservation
     */
    public function getReservation()
    {
        return $this->reservation;
    }
}
