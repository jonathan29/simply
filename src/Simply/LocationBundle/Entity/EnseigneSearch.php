<?php
    /**
     * Created by PhpStorm.
     * User: jonathan
     * Date: 08/12/15
     * Time: 22:08
     */

    namespace Simply\LocationBundle\Entity;

    use Symfony\Component\HttpFoundation\Request;

    class EnseigneSearch
    {

        protected $nom;

        protected $ville;

        protected $theme;

        protected $departement;

        // un tableau public pour être utilisé comme liste déroulante dans le formulaire
        public static $sortChoices = array(
            'nom desc'       => 'nom desc',
            'nom asc'        => 'nom asc',
            'classement desc' => 'classement desc',
            'classement asc' => 'classement asc',
        );

        // définit le champ utilisé pour le tri par défaut
        protected $sort = 'nom';

        // définit l'ordre de tri par défaut
        protected $direction = 'desc';

        // une proprité "virtuelle" pour ajouter un champ select
        protected $sortSelect;

        // le numéro de page par défault
        protected $page = 1;

        // le nombre d'items par page
        protected $perPage = 10;

        public function __construct()
        {
            $this->initSortSelect();
        }

        /**
         * @return mixed
         */
        public function getNom()
        {
            return $this->nom;
        }

        /**
         * @return mixed
         */
        public function getTheme()
        {
            return $this->theme;
        }

        /**
         * @param mixed $theme
         */
        public function setTheme($theme)
        {
            $this->theme = $theme;
        }

        /**
         * @param mixed $libelle
         */
        public function setNom($nom)
        {
            $this->nom = $nom;
        }

        /**
         * @return mixed
         */
        public function getDepartement()
        {
            return $this->departement;
        }

        /**
         * @param mixed $departement
         */
        public function setDepartement(\Simply\LocationBundle\Entity\Departement $departement)
        {
            $this->departement = $departement;
        }

        /**
         * @return mixed
         */
        public function getVille()
        {
            return $this->ville;
        }

        /**
         * @param mixed $ville
         */
        public function setVille(\Simply\LocationBundle\Entity\Ville $ville)
        {
            $this->ville = $ville;
        }

        public function handleRequest(Request $request)
        {
            $this->setPage($request->get('page', 1));
            $this->setSort($request->get('sort', 'publishedAt'));
            $this->setDirection($request->get('direction', 'desc'));
        }

        public function getPage()
        {
            return $this->page;
        }


        public function setPage($page)
        {
            if ($page != NULL) {
                $this->page = $page;
            }

            return $this;
        }

        public function getPerPage()
        {
            return $this->perPage;
        }

        public function setPerPage($perPage = NULL)
        {
            if ($perPage != NULL) {
                $this->perPage = $perPage;
            }

            return $this;
        }

        public function setSortSelect($sortSelect)
        {
            if ($sortSelect != NULL) {
                $this->sortSelect = $sortSelect;
            }
        }

        public function getSortSelect()
        {
            return $this->sort . ' ' . $this->direction;
        }

        public function initSortSelect()
        {
//            $this->sortSelect = 'nom desc';
            $this->sortSelect = $this->sort . ' ' . $this->direction;
        }

        public function getSort()
        {
            return $this->sort;
        }

        public function setSort($sort)
        {
            if ($sort != NULL) {
                $this->sort = $sort;
                $this->initSortSelect();
            }

            return $this;
        }

        public function getDirection()
        {
            return $this->direction;
        }

        public function setDirection($direction)
        {
            if ($direction != NULL) {
                $this->direction = $direction;
                $this->initSortSelect();
            }

            return $this;
        }


    }