<?php

    namespace Simply\LocationBundle\Entity;

    /**
     * EnseigneOption
     */
    class EnseigneOption
    {
        /**
         * @var integer
         */
        private $id;

        /**
         * @var integer
         */
        private $place;

        /**
         * @var string
         */
        private $libelle;

        /**
         * @var boolean
         */
        private $etat;

        /**
         * @var EnseigneOptionImage
         */
        private $image;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set place
         *
         * @param integer $place
         *
         * @return EnseigneOption
         */
        public function setPlace($place)
        {
            $this->place = $place;

            return $this;
        }

        /**
         * Get place
         *
         * @return integer
         */
        public function getPlace()
        {
            return $this->place;
        }

        /**
         * Set libelle
         *
         * @param string $libelle
         *
         * @return EnseigneOption
         */
        public function setLibelle($libelle)
        {
            $this->libelle = $libelle;

            return $this;
        }

        /**
         * Get libelle
         *
         * @return string
         */
        public function getLibelle()
        {
            return $this->libelle;
        }

        /**
         * Set etat
         *
         * @param boolean $etat
         *
         * @return EnseigneOption
         */
        public function setEtat($etat)
        {
            $this->etat = $etat;

            return $this;
        }

        /**
         * Get etat
         *
         * @return boolean
         */
        public function getEtat()
        {
            return $this->etat;
        }

        public function setImage($image)
        {
            $this->image = $image;
        }

        public function getImage()
        {
            return $this->image;
        }

        function __toString()
        {
            return $this->libelle;
        }
    }