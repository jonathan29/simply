<?php

    namespace Simply\LocationBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\HttpFoundation\File\UploadedFile;

    /**
     * Image
     */
    class Image
    {
        /**
         * @var string
         */
        protected $url;

        /**
         * @var string
         */
        protected $alt;

        protected $file;
        protected $tmpFileName;


        /**
         * Set url
         *
         * @param string $url
         *
         * @return HebergementTypeImage
         */
        public function setUrl($url)
        {
            $this->url = $url;

            return $this;
        }

        /**
         * Get url
         *
         * @return string
         */
        public function getUrl()
        {
            return $this->url;
        }

        /**
         * Set alt
         *
         * @param string $alt
         *
         * @return HebergementTypeImage
         */
        public function setAlt($alt)
        {
            $this->alt = $alt;

            return $this;
        }

        /**
         * Get alt
         *
         * @return string
         */
        public function getAlt()
        {
            return $this->alt;
        }

        public function getUploadDir()
        {
            return 'uploads/img';
        }

        protected function getUploadRootDir()
        {
            return __DIR__ . '/../../../../../web/' . $this->getUploadDir();
        }

        public function getFile()
        {
            return $this->file;
        }

        public function setFile(UploadedFile $file)
        {
            $this->file = $file;
            if (NULL !== $this->url) {
                $this->tmpFileName = $this->url;

                $this->url = NULL;
                $this->alt = NULL;
            }
        }

        public function preUpload()
        {
            if (NULL == $this->file) {
                return;
            }
            $this->url = $this->file->guessExtension();
            $this->alt = $this->file->getClientOriginalName();
        }

        public function upload()
        {
            if (NULL == $this->file) {
                die('null');
                return;
            }
            if (NULL !== $this->tmpFileName) {
                $oldFile = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->tmpFileName;
                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }
            }
            $this->file->move($this->getUploadRootDir(),
                $this->id . '.' . $this->url);
        }

        public function preRemoveUpload()
        {
            // On sauvegarde temporairement le nom du fichier, car il    dépend de l'id
            $this->tmpFileName = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->url;
        }

        public function removeUpload()
        {
            // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
            if (file_exists($this->tmpFileName)) {
                // On supprime le fichier
                unlink($this->tmpFileName);
            }
        }

        public function getWebPath()
        {
            return $this->getUploadDir() . '/' . $this->getId() . '.' . $this->getUrl();
        }

    }
