<?php
    /**
     * Created by PhpStorm.
     * User: jonathan
     * Date: 21/11/15
     * Time: 07:06
     */
    namespace Simply\LocationBundle\Menu;

    use Knp\Menu\FactoryInterface;
    use Symfony\Component\DependencyInjection\ContainerAware;

    class MenuBuilder extends ContainerAware
    {
        public function mainMenu(FactoryInterface $factory, array $options)
        {
            $menuOptions = array_merge($options, array(
                'childrenAttributes' => array(
                    'class' => 'nav nav-pills'),
            ));
            $menu = $factory->createItem('main', $menuOptions);
            $menu->addChild('test', array(
                'route' => 'admin_bundles_location_enseigne_list',
            ));

            return $menu;
        }
    }