<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 12/11/2015
 * Time: 19:55
 */

namespace Simply\LocationBundle\Fixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Simply\LocationBundle\Entity\HebergementType;
use Symfony\Component\Form\AbstractType;

class HebergementTypes extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tab = array("mobilhomes", "appartements", "caravane", "villa", "maison", "auberge de
        jeunesse", "Bungalows", "Tente");

        foreach ($tab as $ii => $data) {
            $liste[$ii] = new HebergementType();
            $liste[$ii]->setLibelle($data);

            $manager->persist($liste[$ii]);
//            $this->addReference('hebergements_type'.$ii, $liste[$ii]);

        }

        $manager->flush();

    }

    public function getOrder()
    {
        return 3;
    }


}