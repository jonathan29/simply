<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 17/11/2015
 * Time: 20:45
 */

namespace Simply\LocationBundle\Fixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Simply\LocationBundle\Entity\Pension;
use Simply\LocationBundle\Entity\TourOperateur;

//class TourOperateurs implements FixtureInterface
class Pensions extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tab = array(
            "complète",
            "demi-vomplèteé",
            "petit déjeuner"
        );

        foreach ($tab as $ii => $data) {
            $liste[$ii] = new Pension();
            $liste[$ii]->setLibelle($data[$ii]);

            $manager->persist($liste[$ii]);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}