<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 17/11/2015
 * Time: 20:45
 */

namespace Simply\LocationBundle\Fixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Simply\LocationBundle\Entity\TourOperateur;

//class TourOperateurs implements FixtureInterface
class TourOperateurs extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tab = array(
            "tohapui",
            "mistercamp",
            "vitalyts",
            "vacances",
            "thoapi",
            "tohapui",
            "trip advisor",
            "aventureo",
            "vacances",
            "Look Voyages",
        );

        foreach ($tab as $ii => $data) {
            $liste[$ii] = new TourOperateur();
            $liste[$ii]->setNom($data);
            $liste[$ii]->setMailContact(str_replace(' ', '', "mail@contact" . $data . ".com"));
            $liste[$ii]->setMailDemande(str_replace(' ', '', "mail@demande" . $data . ".com"));
            $liste[$ii]->setChqVacances(true);

            $manager->persist($liste[$ii]);
//            $this->addReference('tour_operateurs'.$ii, $liste[$ii]);
        }

        $manager->flush();


    }

    public function getOrder()
    {
        return 1;
    }
}