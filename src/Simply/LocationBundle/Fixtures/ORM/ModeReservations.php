<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 17/11/2015
 * Time: 20:21
 */

namespace Simply\LocationBundle\Fixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Simply\LocationBundle\Entity\ReservationMode;

class ModeReservations extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $liste = ['réservation', 'demande de disponibilite', 'contacter le camping'];

        foreach ($liste as $ii => $val) {
            $mode = new ReservationMode();
            $mode->setLibelle($val);

            $manager->persist($mode);
        }
        $manager->flush();
//        $this->addReference('modes-reservations', $mode);
    }

    public function getOrder()
    {
        return 2;
    }


}