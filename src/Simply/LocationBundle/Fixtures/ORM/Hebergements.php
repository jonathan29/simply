<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 19/11/2015
     * Time: 13:20
     */

    namespace Simply\LocationBundle\Fixtures\ORM;


    use Doctrine\Common\DataFixtures\AbstractFixture;
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use Simply\LocationBundle\Entity\Hebergement;

    class Hebergements extends AbstractFixture implements OrderedFixtureInterface
    {
        public function load(ObjectManager $manager)
        {
            $liste = [
                "Mobil-home 1 ch.",
                "Mobil-home 2 ch. 4",
                "Mobil-home 3 ch.",
                "Mobil-home 4 ch.",
                "Mobil-home 5 ch.",
                "Mobil-home 6 ch.",
            ];

            foreach ($liste as $ii => $val) {
                $heb = new Hebergement();
                $heb->setLibelle(substr($liste[$ii], 0, 11));
                $heb->setNbChambre(substr($liste[$ii], 11, 1));
                $heb->setDescription($liste[$ii]);

                $heb->setNbPlace(rand(1, 6));
                $heb->setNbPiece(rand(1, 6));

                $manager->persist($heb);
            }
            $manager->flush();


        }

        public function getOrder()
        {
            return 4;
        }

    }