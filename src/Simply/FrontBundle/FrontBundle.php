<?php

    namespace Simply\FrontBundle;

//namespace Simply\FrontBundle;

    use Simply\FrontBundle\DependencyInjection\FrontExtension;
    use Symfony\Component\HttpKernel\Bundle\Bundle;

    class FrontBundle extends Bundle
    {
        public function getContainerExtension()
        {
            return new FrontExtension();
        }
    }
