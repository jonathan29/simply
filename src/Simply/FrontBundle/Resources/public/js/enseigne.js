$(document).ready(function () {
        //reactivie le bouton reserver
        $("select[name='idOffre']").on('change', function (e) {
            e.preventDefault();
            if ($("select[name='idOffre'] option:selected").val() != 'null') {
                   $("button[name='btnReserver']").removeAttr('disabled');
                }
            else if($("select[name='idOffre'] option:selected").val() =='null'){
                $("button[name='btnReserver']").attr('disabled','disabled');
            }


        });  
        
        //bouton ajouter / retirer aux favoris
        $('#btn').click(function () {
            if ($('#btn').hasClass('ajouterFavoris')) {
                addFavoris();
            }
            else {
                delFavoris();
            }
        });

    $(".goto-writereview-pane").click(function (e) {
        e.preventDefault();
        $('#hotel-features .tabs a[href="#hotel-write-review"]').tab('show')
    });

    // editable rating
    $(".editable-rating.five-stars-container").each(function () {
        var oringnal_value = $(this).data("original-stars");
        if (typeof oringnal_value == "undefined") {
            oringnal_value = 0;
        } else {
            //oringnal_value = 10 * parseInt(oringnal_value);
        }
        $(this).slider({
            range: "min",
            value: oringnal_value,
            min: 0,
            max: 5,
            slide: function (event, ui) {
            }
        });
    });

});

$('a[href="#map-tab"]').on('shown.bs.tab', function (e) {
    var center = panorama.getPosition();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
});
$('a[href="#steet-view-tab"]').on('shown.bs.tab', function (e) {
    fenway = panorama.getPosition();
    panoramaOptions.position = fenway;
    panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
    map.setStreetView(panorama);
});
//recupere la lagitude et la longitude de l'enseigne de la vue
var enseigneLong = $('.longitude').data('enseignelongitude');
var enseigneLat = $('.latitude').data('enseignelatitude');
//
var map = null;
var panorama = null;
//var fenway = new google.maps.LatLng(48.855702, 2.292577);
var fenway = new google.maps.LatLng(enseigneLat, enseigneLong);
var mapOptions = {
    center: fenway,
    zoom: 12
};
var panoramaOptions = {
    position: fenway,
    pov: {
        heading: 34,
        pitch: 10
    }
};
function initialize() {
    $("#map-tab").height($("#hotel-main-content").width() * 0.6);
    map = new google.maps.Map(document.getElementById('map-tab'), mapOptions);
    panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
    map.setStreetView(panorama);
}
google.maps.event.addDomListener(window, 'load', initialize);

//Ajoute une enseigne au favoris de l'utilisateur
function addFavoris() {
    var id = $('.iddata').data('enseigneid');
    $.ajax({
        type: 'post',
        url: Routing.generate('ajouter_favoris', {id: id}),
        data: "JSON=" + id,
        success: function () {
            $('.green').toggleClass("green").toggleClass("yellow");
            $('.ajouterFavoris').toggleClass("ajouterFavoris").toggleClass("retirerFavoris");
            $('#btn').html('Retirer des favoris');
        },
        error: function (exception) {
            console.log('Exeption:' + exception)
        }

    })
}
//Retire une enseigne au favoris de l'utilisateur
function delFavoris() {
    var id = $('.iddata').data('enseigneid');
    $.ajax({
        type: 'delete',
        url: Routing.generate('supprimer_favoris', {id: id}),
        data: "JSON=" + id,
        success: function () {
            $('.yellow').toggleClass("yellow").toggleClass("green");
            $('.retirerFavoris').toggleClass("retirerFavoris").toggleClass("ajouterFavoris");
            $('#btn').html('Ajouter aux favoris');
        },
        error: function (exception) {
            console.log('Exeption:' + exception)
        }
    });
}

