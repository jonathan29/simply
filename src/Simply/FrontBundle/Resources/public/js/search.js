$(document).ready(function () {
    $('body').find('[name="sortSelect"] , [name="perPage"] ').on('change', function () {
        var arr_val = $(this).val().split(' ');
        var nbPerPage = $('[name="perPage"]').val();
        // check if keys 0 and 1 exist in array
        if (!arr_val || (arr_val.constructor !== Array && arr_val.constructor !== Object)) {
            return false;
        } else {
            if ((0 in arr_val) && (1 in arr_val)) {
                $('[name="sort"]').val(arr_val[0]);
                $('[name="direction"]').val(arr_val[1]);
                $('[name="page"]').val(1);
                $('[name="perPage"]').val(nbPerPage);
            } else {
                $('[name="sort"]').val('');
                $('[name="direction"]').val('');
            }
        }
        // changer l'ordre de recherche lance une nouvelle recherche
        $('form').submit();
    });


});

//$(document).ready(function () {
//    /**
//     * Created by Jonathan on 09/12/2015.
//     */
//    $('#search').on('submit', function (event) {
//        event.preventDefault();
//        //var dataForm = $(this).serialize();
//        var query = $('#query').val();
//        console.log(query);
//
//        //$.ajax({
//        //    url: Routing.generate('recherche_enseigne_2'),
//        //    type: 'json',
//        //    data: query,
//        //    success: function (data) {
//        //        console.log(data);
//        //    }
//        //});
//        $.post(
//            //$(this).attr('action'),
//            Routing.generate('recherche_enseigne_2'),
//            query,
//            function (data) {
//                console.log(data);
//                $(document).html(data);
//            }
//        );
//    })
//})
