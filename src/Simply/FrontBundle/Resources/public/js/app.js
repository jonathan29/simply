jQuery(document).ready(function () {
    //load bootstrap datepicker
    $('.datepicker').datepicker();

    // Get the ul that holds the collection of tags
    $collectionHolder = $('div#simply_bundles_locationbundle_reservation_participants');

    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addTagLink.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addTagForm($collectionHolder, $newLinkLi);
    });
});
var $collectionHolder;

// setup an "add a tag" link
var $addTagLink = $('<a href="#" class="    "><button class="btn btn-info">Ajouter un nouveau participant</button></a>');
var $newLinkLi = $('<li></li>').append($addTagLink);

function addTagForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    //var prototype = $collectionHolder.data('prototype');
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // var newForm = prototype.replace(/__name__/g, "participant N°" + index);
    var newForm = prototype.replace(/__name__label/, "Participant N°" + index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}

