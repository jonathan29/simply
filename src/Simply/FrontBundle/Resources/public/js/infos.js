jQuery(document).ready(function () {   
    var cp = $('#simply_userbundle_user_cp');

    cp.keyup(function () {
        console.log('pre condidtion succes');
        if (cp.val().length === 5) {
            var form = $(this).closest('form');

            var data = {};

            data[cp.attr('name')] = cp.val();
            console.log("Action :" + form.attr('action'));
            console.log("Method :" + form.attr('method'));

            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: data,
                success: function (html) {
                    $('#simply_userbundle_user_ville').replaceWith(
                        $(html).find('#simply_userbundle_user_ville')
                    );
                }
            })
        }
    });
})