<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 28/12/2015
     * Time: 06:47
     */

    namespace Simply\FrontBundle\Tests\Entity;


    use Proxies\__CG__\Simply\LocationBundle\Entity\Location;
    use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

    class LocationRepositoryTest extends WebTestCase
    {
        private $locationRepository;
        private $offreLocationRepository;


        public function setUp()
        {
            $kernel = static::createKernel();
            $kernel->boot();
            $this->locationRepository = $kernel->getContainer()->get('doctrine.orm.entity_manager')->getRepository
            ('LocationBundle:Location');
            $this->offreLocationRepository = $kernel->getContainer()->get('doctrine.orm.entity_manager')->getRepository
            ('LocationBundle:offreLocation');

        }

        public function testFindPrixMini()
        {
            //on recuopere les resultats de la requete à tester
            //locations les moins chères groupées par enseigne
            $locs = $this->locationRepository->findPrixMini(4);

            //Test que le nombre de resultats soit égale au parametre de la methode
            $this->assertEquals(count($locs), 4);

            $offreLocs = $this->offreLocationRepository->findMoinsCher();

            for($i = 0; $i < count($locs); $i++ ){
                $this->assertEquals($offreLocs[$i]['test'], $locs[$i]['tarifmini']);
            }

            foreach ($locs as $loc) {
                $this->assertInstanceOf(\Simply\LocationBundle\Entity\Location::class, $loc[0]);
                $this->assertInstanceOf(\Simply\LocationBundle\Entity\Enseigne::class, $loc[0]->getEnseigne());
            }
        }


    }