<?php
/**
 * Created by PhpStorm.
 * User: Jonathan
 * Date: 27/12/2015
 * Time: 19:32
 */
namespace Simply\FrontBundle\Tests\Entity;

use Simply\LocationBundle\Entity\Enseigne;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EnseigneTest extends WebTestCase
{
    private $enseigne;
    private $em;

    public function setUp()
    {
        $this->enseigne = new Enseigne();

        $kernel = static::createKernel();
        $kernel->boot();
        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $this->em->beginTransaction();
    }

    public function tearDown()
    {
        $this->em->rollback();
    }

    /**
     * @test
     */
    public function testGetNom()
    {
        $nom = 'le paradis des campings';

        $this->enseigne->setNom($nom);
        $this->assertEquals($nom, $this->enseigne->getNom());
    }

    /**
     * @test
     */
    public function testGetClassement()
    {
        $classement = 3;

        $this->enseigne->setClassement($classement);
        $this->assertEquals($classement, $this->enseigne->getClassement());
    }

    /**
     * @test
     */
    public function testGetVille()
    {
        $ville = $this->em->getRepository('LocationBundle:Ville')->findOneBy(array('libelle' => 'poitiers'));

        $this->assertInstanceOf(\Simply\LocationBundle\Entity\Ville::class, $ville);

        $this->enseigne->setVille($ville);

        $this->assertEquals($ville, $this->enseigne->getVille());
    }

    /**
     * @test
     */
    public function testInsert()
    {
        $enseigneAInserer = $this->createTestEnseigne();

        $this->em->persist($enseigneAInserer);
        $this->em->flush();

        $this->assertEquals($enseigneAInserer, $this->em->getRepository('LocationBundle:Enseigne')->findOneBy(array
        ('nom' => 'Test_enseigne')));
    }

    /**
     * @test
     */
    public function testUpdateEnseigne()
    {
        $enseigneAInserer = $this->createTestEnseigne();

        $this->em->persist($enseigneAInserer);
        $this->em->flush();

        $enseigneToUpate = $this->em->getRepository('LocationBundle:Enseigne')->findOneBy(array('nom' =>
            'Test_enseigne'));

        $ville = $this->em->getRepository('LocationBundle:Ville')->findOneBy(array('libelle' => 'Rennes'));
        $enseigneToUpate->setVille($ville);

        $this->em->persist($enseigneToUpate);

        $enseigneUpdated = $this->em->getRepository('LocationBundle:Enseigne')->findOneBy(array('nom' =>
            'Test_enseigne'));
        $this->assertEquals('Rennes', $enseigneUpdated->getVille()->getLibelle());

    }

    /**
     * @test
     */
    public function testDeleteEnseigne()
    {
        $enseigne = $this->createTestEnseigne();
        $this->em->persist($enseigne);
        $this->em->flush();

        $enseigneToDelete = $this->em->getRepository('LocationBundle:Enseigne')->findOneBy(array
        ('nom' => 'Test_enseigne'));

        $this->em->remove($enseigneToDelete);

        $this->assertNull($this->em->getRepository('LocationBundle:Enseigne')->findOneBy(array
        ('nom' => 'Tes_enseigne')));
    }

    /**
     * @return Enseigne
     */
    private function createTestEnseigne()
    {
        $ville = $this->em->getRepository('LocationBundle:Ville')->findOneBy(array('libelle' => 'Brest'));
        $enseigne = new Enseigne();
        $enseigne->setNom('Test_enseigne');
        $enseigne->setDescription('description de l\'enseigne test');
        $enseigne->setAdresse('2 route de paris');
        $enseigne->setVille($ville);
        $enseigne->setMail('mail@enseigne.com');
        $enseigne->setTel('0123456789');
        $enseigne->setEtat(FALSE);
        $enseigne->setLatitude(35, 000);
        $enseigne->setLongitude(1, 988);
        $enseigne->setClassement(3);
        $enseigne->setDebutSaison(new \DateTime('2016-03-30'));
        $enseigne->setFinSaison(new \DateTime('2016-09-30'));

        return $enseigne;
    }

}