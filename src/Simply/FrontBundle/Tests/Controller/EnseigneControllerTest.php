<?php

namespace Simply\FrontBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EnseigneControllerTest extends WebTestCase
{
    private $client = NULL;
    private $em;

    public function setUp()
    {
        $this->client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'user',
            'PHP_AUTH_PW' => 'user'));

        $kernel = static::createKernel();
        $kernel->boot();

        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
    }
    /**
     * @test
     */
    public function testIndexAction()
    {
        $idEnseigne = 431;
        $crawler = $this->client->request('GET', '/enseigne/' . $idEnseigne);

        $enseigne = $this->em->getRepository('LocationBundle:Enseigne')->find($idEnseigne);

        $this->assertGreaterThan(0, $crawler->filter('html:contains("' . $enseigne->getNom() . '")')->count());
    }
    /**
     * @test
     */
    public function testMethodAjouterFavorisActionOk()
    {
        $this->client->request('POST', '/favoris/add/42', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest'));

        $this->assertEquals('200', $this->client->getResponse()->getStatusCode());
    }
    /**
     * @test
     */
    public function testMethodAjouterFavorisActionNok()
    {
        $this->client->request('GET', '/favoris/add/189', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest'));

        $this->assertEquals('405', $this->client->getResponse()->getStatusCode());
    }
    /**
     * @test
     */
    public function testMethodSupprimerFavorisActionOk()
    {
        $this->client->request('POST', '/favoris/delete/42', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest'));

        $this->assertContains('42', $this->client->getResponse()->getContent());
    }
}
