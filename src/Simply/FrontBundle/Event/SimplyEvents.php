<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 23/12/2015
     * Time: 12:46
     */

    namespace Simply\FrontBundle\Event;


    final class SimplyEvents
    {
        const onDemandeReservation = 'simply.events.reservation';
    }