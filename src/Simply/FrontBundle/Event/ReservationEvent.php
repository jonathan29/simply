<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 23/12/2015
     * Time: 12:46
     */

    namespace Simply\FrontBundle\Event;

    use FOS\UserBundle\Model\UserInterface;
    use Simply\LocationBundle\Entity\Reservation;
    use Symfony\Component\EventDispatcher\Event as Event;

    class ReservationEvent extends Event
    {
        protected $user;
        protected $reservation;

        public function __construct(UserInterface $userInterface, Reservation $reservation)
        {
            $this->reservation = $reservation;
            $this->user = $userInterface;
        }

        public function getReservation()
        {
            return $this->reservation;
        }

        public function getUser()
        {
            return $this->user;
        }

    }