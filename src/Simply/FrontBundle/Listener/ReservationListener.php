<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 23/12/2015
     * Time: 12:52
     */

    namespace Simply\FrontBundle\Listener;

    use FOS\UserBundle\Model\UserInterface;
    use Simply\FrontBundle\Event\ReservationEvent;
    use Simply\LocationBundle\Entity\Reservation;
    use Symfony\Bundle\TwigBundle\TwigEngine;

    class ReservationListener
    {
        protected $mailer;
        protected $template;

        public function __construct(\Swift_Mailer $mailer, TwigEngine $template)
        {
            $this->mailer = $mailer;
            $this->template = $template;
        }

        private function sendEmail(UserInterface $user, Reservation $reservation)
        {
            $message = \Swift_Message::newInstance()
                ->setSubject("Demande de réservation pris en compte")
                ->setFrom('jon.roue@gmail.com')
                ->setTo($user->getEmail())
                ->setBody($this->template->render('FrontBundle:Email:demande-reservation.html.twig', array(
                    'reservation' => $reservation
                )));

            $this->mailer->send($message);
        }

        public function onDemandeReservation(ReservationEvent $event)
        {
            $user = $event->getUser();
            $resa = $event->getReservation();

            $this->sendEmail($user, $resa);
        }

    }