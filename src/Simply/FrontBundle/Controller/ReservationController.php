<?php
    /**
     * Created by PhpStorm.
     * User: Jonathan
     * Date: 03/12/2015
     * Time: 10:04
     */

    namespace Simply\FrontBundle\Controller;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Simply\FrontBundle\Event\ReservationEvent;
    use Simply\FrontBundle\Event\SimplyEvents;
    use Simply\LocationBundle\Entity\Reservation;
    use Simply\LocationBundle\Form\Type\ReservationType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Session\Session;

    /**
     * Class ReservationController
     * @package Simply\FrontBundle\Controller
     *
     * @Route("/reservation")
     */
    class ReservationController extends Controller
    {

        /**
         * @param $id
         * @return \Symfony\Component\HttpFoundation\Response
         *
         * @Route("/{id}",name="reservation_voir", requirements={"id" = "\d+"})
         */
        public function voirAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $reservation = $em->getRepository('LocationBundle:Reservation')->findReservationInfo($id);

//        $participants = $reservation->getParticipants();
            if (!$reservation) {
                throw $this->createNotFoundException("pas de réservation avec l'id");
            }

            return $this->render('FrontBundle:Reservation:voir.html.twig', array(
                    'reservation' => $reservation,
//                'participants' => $participants
                )
            );
        }

        /**
         * @param Request $request
         * @param         $id
         * @return \Symfony\Component\HttpFoundation\RedirectResponse
         *
         * @Route("/delete/{id}",name="reservation_archive",requirements={"id" = "\d+"})
         * ParamConverter("reservation",class="LocationBundle:Reservation",options={"id"="id"})
         */
        public function supprimerAction(Request $request, Reservation $reservation/*$id*/)
        {
            $em = $this->getDoctrine()->getManager();

//            $reservation = $em->getRepository('LocationBundle:Reservation')->find($id);
            if ($reservation == NULL) {
                throw $this->createNotFoundException();
            }

            $reservation->setArchive(TRUE);

            $em->persist($reservation);
            $em->flush();

            $this->addFlash('success', 'La réservation a bien supprimée');

            return $this->redirect($this->generateUrl('mes_reservations'));
        }

        /**
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
         *
         * @Route("/creer",name="reservation_creer")
         */
        public function createAction(Request $request)
        {
            $em = $this->getDoctrine()->getManager();
            $id = $request->query->get('idOffre');
            $session = $this->get('session');

            $reservation = new Reservation();
            $form = $this->createForm(new ReservationType(), $reservation);

            if ($request->getMethod() == 'GET') {
                $offreLoc = $em->getRepository('LocationBundle:OffreLocation')->findOneById($id);
                $session->set('idOffre', $id);

                $reservation->setEtat($em->getRepository('LocationBundle:Etat')->findOneBy(array('statut' => 0)));
                $reservation->setUser($this->getUser());
                $reservation->setOffreLocation($offreLoc);

                $em->persist($reservation);
                $em->flush();
            }

            if ($request->getMethod() == 'POST') {
                $offreLoc = $em->getRepository('LocationBundle:OffreLocation')->findOneById($session->get('idOffre'));
                $resa = $em->getRepository('LocationBundle:Reservation')->findOneBy(array(
                    'offreLocation' => $offreLoc,
                    'user'          => $this->getUser(),
                    'etat'          => $etat = $em->getRepository('LocationBundle:Etat')->findOneBy(array('statut' =>
                                                                                                              0)),
                ));
                if ($resa == NULL) {
                    throw $this->createNotFoundException("pas de resa trouve");
                }

                $form->handleRequest($request);

                // reservation en attente
                $etat = $em->getRepository('LocationBundle:Etat')->findOneBy(array('statut' => 1));
                $resa->setEtat($etat);
                $resa->setUser($this->getUser());
                $resa->setOffreLocation($offreLoc);

                if ($form->isValid()) {
                    //recupere l'etat en attenete
                    $event = new ReservationEvent($this->getUser(), $resa);
                    $this->get('event_dispatcher')->dispatch(SimplyEvents::onDemandeReservation, $event);

                    $em->persist($resa);
                    $em->flush();

                    $this->addFlash('success', 'La réservation a bien été prise en comte ');

                    return $this->redirect($this->generateUrl('mes_reservations'));
                }
            }

            $enseigne = $em->getRepository('LocationBundle:Enseigne')->findEnseigneReservation($id);
            if (!$enseigne) {
                throw $this->createNotFoundException(" aucune enseigne ne correspond à cette location");
            }

            return $this->render('FrontBundle:Reservation:reservation-formulaire.html.twig', array(
                'form'          => $form->createView(),
                'offreLocation' => $offreLoc,
                'idoffre'       => $id,
                'enseigne'      => $enseigne,
            ));
        }

    }