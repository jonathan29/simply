<?php
    /**
     * Created by PhpStorm.
     * User: jonathan
     * Date: 01/12/15
     * Time: 20:38
     */
    namespace Simply\FrontBundle\Controller;

    use Simply\UserBundle\Form\UserType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Request;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


    /**
     * Class MonCompteController
     * @package Simply\FrontBundle\Controller
     *
     * @Route("/mon-compte")
     */
    class MonCompteController extends Controller
    {
        /**
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\Response
         * @Route("/mes-infos",name="moncompte")
         */
        public function indexAction(Request $request)
        {
            //$request = $this->get('request');
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();
            $form = $this->createForm(new UserType($em), $user);

            if ($request->getMethod() == 'POST') {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $em->persist($user);
                    $em->flush();
                    $this->addFlash('success', 'Les modifications ont bien été prise en compte');
                }
            }

            return $this->render('FrontBundle:MonCompte:mes-infos.html.twig', array(
                'form' => $form->createView(),
            ));
        }

        /**
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\Response
         *
         * @Route("/favoris",name="camping_favoris")
         */
        public function favorisAction(Request $request)
        {
            $enseignes = $this->getUser()->getEnseignes();

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $enseignes,
                $request->query->getInt('page', 1),
                10
            );

            return $this->render('FrontBundle:MonCompte:favoris.html.twig', array(
                'enseignes'  => $enseignes,
                'pagination' => $pagination));
        }

        /**
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\Response
         *
         * @Route("/mes-reservations",name="mes_reservations")
         */
        public function reservationsAction(Request $request)
        {
            //attention ne pas executer la requete dans la methode du repository
            $reservations = $this->getDoctrine()->getRepository('LocationBundle:Reservation')
                ->findUsersReservations($this->getUser());

            //pagination
            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $reservations, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                10/*limit per page*/
            );


            return $this->render('FrontBundle:MonCompte:mes-reservations.html.twig',
                array('reservations' => $reservations,
                      'pagination'   => $pagination));
        }

//        public function villeAction(Request $request)
//        {
//            $query = $request->get('search', NULL);
//
//            // notre index est directement disponible sous forme de service
//            $index = $this->container->get('fos_elastica.index.simply.ville');
//
//            $searchQuery = new \Elastica\Query\QueryString();
//            $searchQuery->setParam('query', $query);
//
//            // nous forçons l'opérateur de recherche à AND, car on veut les résultats qui
//            // correspondent à tous les mots de la recherche, plutôt qu'à au moins un
//            // d'entre eux (opérateur OR)
//            $searchQuery->setDefaultOperator('AND');
//
//            // on exécute une requête de type "fields", qui portera sur les colonnes "name"
//            // et "zipcode" de l'index
//            $searchQuery->setParam('fields', array(
//                'libelle',
//                'cp',
//            ));
//
//            // exécution de la requête, limitée aux 10 premiers résultats
//            $results = $index->search($searchQuery, 10)->getResults();
//            $data = array();
//
//            // on arrange les données des résultats...
//            foreach ($results as $result) {
//                $source = $result->getSource();
//                $data[] = array(
//                    'suggest' => $source['cp'] . ' ' . $source['libelle'],
//                    'cp'      => $source['cp'],
//                    'ville'   => $source['libelle'],
//                );
//            }
//
//            // ...avant de les retourner en json
//            return new JsonResponse($data, 200, array(
//                'Cache-Control' => 'no-cache',
//            ));
//        }
//
//
//        public function villesAction(Request $request, $cp)
//        {
//            $em = $this->getDoctrine()->getManager();
//            $ville = $em->getRepository('LocationBundle:Ville')->findBy(array('cp' => $cp));
//
//            $villes = array();
//            if ($ville) {
//                foreach ($ville as $v) {
//                    $villes[] = $v->getLibelle();
//                }
//            } else {
//                $ville = NULL;
//            }
//            $response = new JsonResponse();
//
//            return $response->setData(array('ville' => $villes));
//
//        }
    }