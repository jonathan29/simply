<?php
    namespace Simply\FrontBundle\Controller;

    use Elastica\Query;
    use Pagerfanta\Adapter\ArrayAdapter;
    use Pagerfanta\Pagerfanta;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
    use Simply\LocationBundle\Entity\Enseigne;
    use Simply\LocationBundle\Entity\Location;  
    use Simply\LocationBundle\Entity\EnseigneSearch;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Request;

    class EnseigneController extends Controller
    {

        /**
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\Response
         * @Route("/enseigne/{id}",name="enseigne_voir", requirements={"id"="\d+"})
         * @ParamConverter("enseigne", class="LocationBundle:Enseigne",options={"id"="id"})
         */
        public function indexAction(Request $request, Enseigne $enseigne)
        {
            //recupere le gestionnaire d'entité doctrine
            $em = $this->get('doctrine.orm.default_entity_manager');
            
            //retourne les offres de locations les moins cheres, groupées par enseigne
            $offreMoinCher = $em->getRepository('LocationBundle:OffreLocation')
                ->findMoinsCherByEnseigne($enseigne);


            return $this->render('FrontBundle:Enseigne:enseigne.html.twig',
                array(
                    'enseigne'      => $enseigne,
                    'offreMoinsCher' => $offreMoinCher,
                    'favoris'       => $this->getUser()->getEnseignes())
            );
        }

        /**
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\Response
         *
         * @Route("/favoris/add/{id}",name="ajouter_favoris",options={"expose"=true}, requirements={"id" = "\d+"})
         * @Method({"POST"})
         * @ParamConverter("enseigne", class="LocationBundle:Enseigne", options={"id" = "id"})
         * @Security("has_role('ROLE_USER')")
         */
        public function ajouterFavorisAction(Request $request, Enseigne $enseigne)
        {
            //Test si la requete est une requete ajax
            if ($request->isXmlHttpRequest()) {

                $em = $this->get('doctrine.orm.default_entity_manager');

                //Crée un objet JSONResponse
                $response = new JsonResponse();
                
                if (!$enseigne) {
                    return $response->setStatusCode(404)->setData(array('enseigne' => 'Enseigne inexistante'));
                }
                //Récupère l'utilisateur connecté
                $user = $this->get('security.token_storage')->getToken()->getUser();

                //ajoute l'enseigne au favoris de l'utilisateur
                $user->addEnseigne($enseigne);

                $em->flush();
               
                $response->setStatusCode(200);

                return $response->setData(array('enseigne' => $enseigne->getNom()));
            }
        }

        /**
         * @param Request $request
         * @param         $id
         * @return JsonResponse|\Symfony\Compone    nt\HttpFoundation\RedirectResponse
         * @throws \Exception
         * @Route("/favoris/delete/{id}",name="supprimer_favoris",options={"expose"=true}, requirements={"id" = "\d+"})
         * @ParamConverter("enseigne",class="LocationBundle:Enseigne",options={"id"="id"})
         */
        public function supprimerFavorisAction(Request $request, Enseigne $enseigne)
        {
            $em = $this->getDoctrine()->getManager();
            if (!$enseigne) {
                throw $this->createNotFoundException(
                    'Aucune enseigne trouvée comportant l\'id ' . $id
                );
            }
            $user = $this->getUser();
            if (!$user) {
                throw $this->createNotFoundException(
                    'pas de user'
                );
            }
            $user->removeEnseigne($enseigne);
            $em->flush();
            if ($request->isXmlHttpRequest()) {
                $response = new JsonResponse();

                return $response->setData(array('enseigne' => ($enseigne->getId())));
            } else {
                $this->addFlash('success', 'Le camping a bien été retiré des favoris');

                return $this->redirect($this->generateUrl('camping_favoris'));
            }
        }


        /**
         * @param Request $request
         * @param         $page
         * @return \Symfony\Component\HttpFoundation\Response
         *
         * @Route("/enseignes/{page}",name="enseigne_recherche",requirements={"page"="\d+"},defaults={"page"=1})
         * @Method({"GET"})
         */
        public function rechercheAction(Request $request, $page)
        {
            $enseigneSearch = new EnseigneSearch();

            $enseigneSearchForm = $this->get('form.factory')
                ->createNamed(
                    '',
                    'enseigne_search_type',
                    $enseigneSearch,
                    array(
                        'action' => $this->generateUrl('enseigne_recherche'),
                        'method' => 'GET',
                    )
                );
            $enseigneSearchForm->handleRequest($request);
            $enseigneSearch = $enseigneSearchForm->getData();

            $elasticaManager = $this->container->get('fos_elastica.manager');
            $results = $elasticaManager->getRepository('LocationBundle:Enseigne')->search($enseigneSearch);

            //Mise en place de la pagination
            $adapter = new ArrayAdapter($results);
            $pager = new Pagerfanta($adapter);
            $pager->setMaxPerPage($enseigneSearch->getPerPage());
            $pager->setCurrentPage($page);

            $prices = array();
            $tab = count($results);
            for($i =0 ; $i < $tab; $i++){
                $prix = $this->getDoctrine()->getManager()->getRepository('LocationBundle:Enseigne')->findMinPrix($results[$i]);
                // $prices[$i]['enseigne_id']=$results[$i]->getId();
                $prices[$i] = $prix;
            }

            return $this->render('FrontBundle:Enseigne:enseigne-search-form.html.twig', array(
                'results' => $pager->getCurrentPageResults(),
                'form'    => $enseigneSearchForm->createView(),
                'pager'   => $pager,
                'prices'=>$prices
            ));
        }

    }

