<?php

    namespace Simply\FrontBundle\Controller;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
    use Simply\LocationBundle\Entity\EnseigneSearch;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;


    class IndexController extends Controller
    {
        /**
         * @return \Symfony\Component\HttpFoundation\Response
         * @Route("/",name="front_homepage")
         * @Security("has_role('ROLE_USER')")
         * @Template("FrontBundle:Index:index.html.Twig")
         */
        public function indexAction()
        {
            $em = $this->get('doctrine.orm.default_entity_manager');

            //Création formulaire de recherche
            $enseigneSearch = new EnseigneSearch();

            $enseigneSearchForm = $this->get('form.factory')
                ->createNamed(
                    '',
                    'enseigne_search_type',
                    $enseigneSearch,
                    array(
                        'action' => $this->generateUrl('enseigne_recherche'),
                        'method' => 'GET',
                    )
                );

            //locations moins cheres
            $locationsPrixMini = $em->getRepository('LocationBundle:Location')->findPrixMini(4);

            //dernieres locations - nouveautés
            $locationsNouveautes = $em->getRepository('LocationBundle:Location')->findLast(4);

/*            return $this->render('FrontBundle:Index:index.html.twig',
                array(
                    'locationPrixMini'   => $locationsPrixMini,
                    'locationNouveautes' => $locationsNouveautes,
                    'form'               => $enseigneSearchForm->createView(),
                )
            );*/
            return array(
                'locationPrixMini'   => $locationsPrixMini,
                'locationNouveautes' => $locationsNouveautes,
                'form'               => $enseigneSearchForm->createView(),
            );
        }

        /**
         * @return \Symfony\Component\HttpFoundation\Response
         * @Template("FrontBundle:Index:footer.html.twig")
         */
        public function footerAction()
        {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $locations = $em->getRepository('LocationBundle:Location')->findPrixMini(9);

            /*            return $this->render('FrontBundle:Index:footer.html.twig', array(
                            'locations' => $locations,
                        ));*/

            return array('locations' => $locations);
        }
    }

